/*
    思路
    #1，取個位數的數字
    n % 10 ，例如，91 得 1

    #2，取十位數的數字
    n / 10) | 0，例如，91 得 9

    #3，流程
        step1，先取得當前數值的個位數的數字，並放進對應的 queue 中
               例如，93，個位數為3，放進 queues[3]
        step2，再取得當前數值的十位數的數字，並放進對應的 queue 中
               例如，93，十位數為9，放進 queues[9]
*/
const Queue = require("./queue")

function createQueues() {
    let queues = []

    for (var i = 0; i < 10; ++i) {
        queues.push(new Queue())
    }

    return queues
}

function distribueToQueue(queues, numList, digit) {
    // 依照 個位數的數字，分裝到對應的 queue
    for (var i = 0; i < numList.length; ++i) {

        if (digit == 1) {
            // 取個位數的數字
            queues[numList[i] % 10].enqueue(numList[i])
        } else {
            // 取十位數的數字
            queues[(numList[i] / 10) | 0].enqueue(numList[i])
        }
    }

    return queues
}

function queuesToArray(queues) {
    const numList = []

    for (var i = 0; i < queues.length; ++i) {
        let itemCount = queues[i].length()

        if (itemCount != 0) {
            for (var j = 0; j < itemCount; ++j) {
                numList.push(queues[i].dequeue())
            }
        }
    }

    return numList
}

function sort_by_queue(numList) {
    console.log("===========")
    var queues = createQueues()

    // 依個位數的數字排序
    queues = distribueToQueue(queues, numList, 1)
    firstList = queuesToArray(queues)
    console.log(`first-sort: ${firstList}`)

    // 依十位數的數字排序
    queues = distribueToQueue(queues, firstList, 2)
    console.log(`result-sort: ${queuesToArray(queues)}`)
}

/*
first-sort: 91,31,  92,22,  85,15,35,  46
result-sort: 15,22,31,35,46,85,91,92
*/
var numList = [91, 46, 85, 15, 92, 35, 31, 22]
sort_by_queue(numList)

/*
first-sort: 70,  51,21,41,31,  72,  93,  45,  16,  27
result-sort: 16,21,27,31,41,45,51,70,72,93
*/
var numList = [45, 72, 93, 51, 21, 16, 70, 41, 27, 31]
sort_by_queue(numList)

/*
first-sort: 71,  84,54,  15,  76,6,  77,  79,69,99
result-sort: 6,15,54,69,71,76,77,79,84,99
*/
var numList = [76, 77, 15, 84, 79, 71, 69, 99, 6, 54]
sort_by_queue(numList)
