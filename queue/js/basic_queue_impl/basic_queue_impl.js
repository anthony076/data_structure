
const test = require("tape")

function Queue() {
    this.dataStore = []

    this.enqueue = function (el) {
        this.dataStore.push(el)
    };

    this.dequeue = function () {
        // 取出頭元素
        return this.dataStore.shift()
    };

    this.front = function () {
        return this.dataStore[0]
    };

    this.end = function () {
        return this.dataStore[this.dataStore.length - 1]
    };

    this.length = function () {
        return this.dataStore.length
    }

    this.toString = function () {
        return this.dataStore.join(",")
    }

    this.clear = function () {
        this.dataStore = []
    }
}

const q = new Queue()

test("test enqueue", (t) => {
    q.enqueue(1)
    q.enqueue(3)
    q.enqueue(5)

    t.isEqual(q.toString(), "1,3,5")

    t.end()
})

test("test dequeue", (t) => {
    q.dequeue()
    t.isEqual(q.toString(), "3,5")

    t.end()
})

test("test front", (t) => {
    t.isEqual(q.front(), 3)
    t.end()
})

test("test end", (t) => {
    t.isEqual(q.end(), 5)
    t.end()
})

test("test length", (t) => {
    t.isEqual(q.length(), 2)
    t.end()
})

test("test length", (t) => {
    q.clear()
    t.isEqual(q.length(), 0)
    t.end()
})

