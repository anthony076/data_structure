const Queue = require("./queue")

function CircularQueue() {
    const q = new Queue()

    this.enqueue = function (el) {
        q.enqueue(el)
        return this
    }

    this.move = function () {
        q.enqueue(q.dequeue())
        return this
    }

    this.toString = function () {
        console.log(q.toString())
        return this
    }
}

const cq = new CircularQueue()
cq.enqueue("a")
cq.enqueue("b")
cq.enqueue("c")
cq.toString()   // a,b,c

cq.move().toString()    // b,c,a
cq.move().toString()    // c,a,b
cq.move().toString()    // a,b,c