
function Queue() {
    this.dataStore = []

    this.enqueue = function (el) {
        this.dataStore.push(el)
    };

    this.dequeue = function () {
        // 取出頭元素
        return this.dataStore.shift()
    };

    this.front = function () {
        return this.dataStore[0]
    };

    this.end = function () {
        return this.dataStore[this.dataStore.length - 1]
    };

    this.length = function () {
        return this.dataStore.length
    }

    this.toString = function () {
        return this.dataStore.join(",")
    }

    this.clear = function () {
        this.dataStore = []
    }
}

module.exports = Queue