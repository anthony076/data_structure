
function Element(data, priority) {
    this.data = data
    this.priority = priority
}

function PriorityQueue() {
    this.dataStore = []

    this.enqueue = function (el, priority) {
        let newElement = new Element(el, priority)

        // ==== 尋找插入的位置 ====
        let lastIndex = this.dataStore.length - 1;
        let newPriority = newElement.priority

        if (this.dataStore.length == 0) {
            // case1, queue 中沒有任何元素時，直接插入
            this.dataStore.push(newElement)

        } else if (newPriority >= this.dataStore[lastIndex].priority) {
            // case2, 新元素的 priority >= queue 尾元素的 priority 時，插入到最後
            this.dataStore.push(newElement)

        } else {
            // case3, 新元素的 priority < queue 尾元素的 priority 時，進入比較模式
            // 此區域不會有大數，大數會在 case2 被攔截，並插入到最後

            // 定位到 queue 的尾元素
            let index = lastIndex

            while (index > 0) {
                if (newPriority < this.dataStore[index].priority) {
                    --index
                } else {
                    break
                }
            }

            if (index == 0) {
                // index == 0，當前元素是最小數
                this.dataStore.splice(0, 0, newElement)
            } else {
                // index != 0，當前元素不是最小數
                this.dataStore.splice(index, 0, newElement)
            }
        }
    };
}

const pq = new PriorityQueue()

pq.enqueue("a", 2)
pq.enqueue("b", 1)
pq.enqueue("c", 3)
pq.enqueue("d", 0)
pq.enqueue("e", 3)
pq.enqueue("f", 1)
pq.enqueue("g", 3)
pq.enqueue("h", 0)

console.log(pq.dataStore)