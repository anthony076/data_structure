
function QueueElement(element, priority) {
    this.element = element;
    this.priority = priority;
}

function PriorityQueue() {
    this.items = [];

    this.enqueue = function (element, priority) {
        var newElement = new QueueElement(element, priority);

        if (this.items.length == 0) {
            this.items.push(newElement);
        } else {
            var added = false;

            for (var i = 0; i < this.items.length; i++) {
                if (newElement.priority < this.items[i].priority) {
                    this.items.splice(i, 0, newElement);
                    added = true;
                    break;
                }

            } if (!added) {
                this.items.push(newElement);
            }
        }
    }
}

let pq = new PriorityQueue()

pq.enqueue("a", 2)
pq.enqueue("b", 1)
pq.enqueue("c", 3)
pq.enqueue("d", 0)
pq.enqueue("e", 3)
pq.enqueue("f", 1)
pq.enqueue("g", 3)
pq.enqueue("h", 0)

console.log(pq.items)