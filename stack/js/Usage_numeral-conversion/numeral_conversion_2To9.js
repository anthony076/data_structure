
/* 
10進制 -> n 進制，轉換的思路
a/b 取商數
a%b 取餘數

8 / 2 = 4 .. 0
4 / 2 = 2 .. 0
2 / 2 = 1 .. 0

step1, 將每次除法的結果取餘數後壓入 stack，
step2, 將每次除法的結果取商數，當作下一次的被除數
step3, 當商數 < b 的時候停止，
step4，棧頂是高位，棧頂是低位
*/

const Stack = require("./stack")
const test = require("tape")

function convert(n, base) {
    s = new Stack()
    let result = ""

    // step1 calculate
    while (!(n < base)) {
        r = n % base

        s.push(r)
        n = (n / base)
    }

    s.push(n)

    // step2 get value
    while (s.length() > 0) {
        result += s.pop()
    }

    return result

}

test("test function", (t) => {
    t.equal(convert(8, 2), "1000")
    t.equal(convert(8, 8), "10")
    t.end()
})
