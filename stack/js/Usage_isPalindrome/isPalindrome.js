
/* 
思路
    輸入字串，例如 dad，
    特徵，具回文性質字串，其反向後的字串內容與原字串相同
    因此，利用 stack 取得反轉後的字串，再與原字串比較是否相等
*/

const Stack = require("./stack")
const test = require("tape")

function isPalindrome(str) {
    const s = new Stack()
    let newStr = ""

    // push to stack
    for (char of str) {
        if (/\w+/.test(char)) {
            tmp = char.toLowerCase()
            newStr += tmp
            s.push(tmp)
        }
    }

    // get reversed-word and compare to input
    let rword = ""
    while (s.length() > 0) {
        rword += s.pop()
    }

    return rword == newStr
}

test("test isPalindrome", (t) => {
    t.ok(isPalindrome("dad"))
    t.ok(isPalindrome("daad"))
    t.ok(isPalindrome("racecar"))
    t.ok(isPalindrome("A man, a plan, a canal: Panama"))
    t.notOk(isPalindrome("abc"))
    t.notOk(isPalindrome("dabcad"))
    t.end()
})