/*
    思路
    #1 遇到 ( 的時候入棧，遇到 ) 的時候出棧
    #2 出棧的狀況分析
            2-1，( 數量 大於 ) 數量，解析結束但 stack.length != 0
            2-2，( 數量 小於 ) 數量，遇到 ) 要 pop 的時候，stack.length 已經為 0
            2-3，( 數量 等於 ) 數量，解析結束且 stack.length == 0
*/
const Stack = require("./stack")

function detect(str) {
    const s = new Stack()

    for (var i = 0; i < str.length; ++i) {
        char = str[i];

        if (char == "(") {
            s.push(char)
        } else if (char == ")") {
            if (s.length() != 0) {
                s.pop()
            } else {
                // 狀況 2-2，( 數量 小於 ) 數量
                return i + 1
            }
        } else { }
    }

    if (s.length() != 0) {
        // 狀況 2-1，( 數量 大於 ) 數量
        return i + 1
    } else {
        // 狀況 2-3，( 數量 等於 ) 數量
        return -1
    }
}

console.log(detect("2.3+23/12+(3.14159×0.24"))      // 24
console.log(detect("2.3+23/12+(3.14159×0.24)"))     // -1
console.log(detect("(2.3+(23/12)+(3.14159×0.24)"))  // 28