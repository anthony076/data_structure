/* 
關係
    # index = this.top - 1
    # length = this.top
*/

const test = require("tape")

function Stack() {
    this.dataStore = []
    // index = 0 , this.top = 1
    // index = 1 , this.top = 2
    this.top = 0

    this.push = function (el) {
        // 注意 ++this.top 和 this.top++ 的差異
        // this.top++ 會先將 this.top 傳遞給 array 後再+1
        // 因為 dataStore 是 array 是從 0 開始，
        this.dataStore[this.top++] = el
    };

    this.pop = function () {
        // index = this.top - 1，因此先 -1 後，再取 this.top 的值
        // 注意，pop() 是利用 top 指針的移動模擬取出的效果，沒有真的把元素刪除
        return this.dataStore[--this.top]
    };

    this.peek = function () {
        // 只是利用 top 指針，不修改 top 指針本身
        // pop() 和 push() 才會修改 top 指針
        return this.dataStore[this.top - 1]
    };

    this.length = function () {
        //return this.top + 1     // 錯誤，棧沒有元素時，應該返回 0
        // top == length，沒有元素時應該返回 0
        return this.top
    }

    this.clear = function () {
        return this.top = 0
    }
}

// ==========
// 測試
// ==========

var s = new Stack()

test("test init", (t) => {
    t.equal(s.length(), 0)
    t.end()
})

test("test push", (t) => {
    s.push(1)
    s.push(3)
    s.push(5)
    t.deepEqual(s.dataStore, [1, 3, 5])
    t.equal(s.length(), 3)
    t.equal(s.top, 3)
    t.end()
})

test("test pop", (t) => {
    t.equal(s.pop(), 5)
    t.notDeepEqual(s.dataStore, [1, 3])
    t.equal(s.length(), 2)
    t.equal(s.top, 2)

    t.end()
})

test("test peak", (t) => {
    t.equal(s.peek(), 3)
    t.equal(s.length(), 2)
    t.equal(s.top, 2)

    t.end()
})

test("test clear", (t) => {
    s.clear()
    t.equal(s.length(), 0)
    t.equal(s.top, 0)

    t.end()
})





