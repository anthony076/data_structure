/*
    
    題目
        將中綴表達式 3+4 變成候綴表達式後，進行運算

    分析
    4 + 3 * 2
    numStack : 4   3   2
    opStack  :   +   *

    條件，先乘除，後加減
    
    思路1
        # 遇到 number => 壓入 numStack
        # 遇到 operator => 
            if + - => 壓入 opStack
            if * / => 計算 => 結果壓入 numStack

    思路2，
        # 條件，不能在 operater 的時候計算，因為尚未取得下一個 number 值，無法計算
        # 2-1, 當前位置是 operater 時，將 operator 壓入 opStack
          2-2, 當前位置是數字時，檢查 opStack 棧頂是否是高優先及操作符(* /)
                - 若是 * /，將當前數值壓入 numStack 並進行運算，將運算結果壓入 numStack
                - 若不是 * /，將當前數值 壓入 numStack
        # 2-3，解析完畢後，opStack 中已經沒有 * /，只有 + - ，進行連續運算
*/
const Stack = require("./stack")

let numStack = new Stack()
let opStack = new Stack()

let isOperator = (char) => ["+", "-", "*", "/"].some(el => el == char)
let isHighPiority = (char) => ["*", "/"].some(el => el == char)

let calculate = (numStack, opStack) => {
    let result = 0

    // 注意，第1個彈出來的是減數()，第2個彈出來的是被減數()，
    // 4-3 和 3-4 的結果是不同的
    // 1/2 和 2/1 的結果是不同的
    let n2 = numStack.pop()
    let n1 = numStack.pop()
    op = opStack.pop()

    switch (op) {
        case "+":
            result = Number(n1) + Number(n2)
            break
        case "-":
            result = Number(n1) - Number(n2)
            break
        case "*":
            result = Number(n1) * Number(n2)
            break
        case "/":
            result = Number(n1) / Number(n2)
            break
        default:
            throw new Error(`operator:${op} in not defined`)
    }

    numStack.push(result)
}

function cal_express(str) {

    // ==== 解析表達式，將 num 和 op 分別放在兩個獨立的 stack， ====
    // 若有 * / )，優先計算
    for (var i = 0; i < str.length; ++i) {
        char = str[i]

        // ==== 檢查當前位置是 num 或是 op ====
        if (isOperator(char) | char == "(") {
            // ==== current char is +-*/ ====
            // 2-1, 當前位置是 operater 時，將 operator 壓入 opStack
            opStack.push(char)
        } else if (char == ")") {
            // ==== current char is ) ====
            // 對 () 優先進行運算，以( 當作結束運算的訊號
            while (opStack.peek() != "(") {
                calculate(numStack, opStack)
            }

            // 將 ( 符號彈出
            opStack.pop()
        } else {
            // ==== current char is number ====
            //2-2, 當前位置是數字時，先將當前數字壓入 numStack 
            if (opStack.peek() == "-") {
                // ===== 負值轉換 ====
                // 將 -3 改成 + (-3)，避免運算的邏輯錯誤
                // 例如，-3+10 = 7
                // 運算是和數值分開後，會變成 -(3+7) = 10
                // 因此，-3 改成 +(-3)
                numStack.push("-" + char)   // 將負數改成
                opStack.pop()
                opStack.push("+")
            } else (
                numStack.push(char)
            )

            // ==== op 是 * / 時，先進行運算 ====
            //檢查 opStack 棧頂是否是高優先及操作符
            if (isHighPiority(opStack.peek())) {
                calculate(numStack, opStack)
            }
        }
    }

    // ==== op 中已經沒有高優先級的 op，只剩下 + -，進行連續運算 ====
    while (numStack.length() != 1) {
        calculate(numStack, opStack)
    }

    return numStack.pop()
}

console.log(cal_express("4+3*2*2"))         // 16
console.log(cal_express("4+3*2*2+4"))       // 20
console.log(cal_express("4+3*2*2+4-1*3"))   // 17
console.log(cal_express("3*4*5"))           // 60
console.log(cal_express("1+2+3+4+5"))       // 15
console.log(cal_express("4+5-3*9-(2+3)"))   // -23
console.log(cal_express("4+5-3*9-(2+3*4+1)"))   // -33