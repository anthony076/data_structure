
function findMaxCommonString(s1, s2) {
    let T = []

    // ==== step1，透過狀態轉移公式建立表格 ====
    for (var i = 0; i < s1.length; i++) {
        T[i] = []
        for (var j = 0; j < s2.length; j++) {
            // 第一橫列
            if (i === 0 && j === 0) {
                //(0,0) 點的判斷
                T[i][j] = (s1[i] == s2[j]) ? 1 : 0
            } else if (i === 0) {
                //(0,j) 點的判斷
                if (s1[i] == s2[j]) {
                    T[i][j] = 1
                } else {
                    T[i][j] = T[i][j - 1]
                }
            } else {
                // (i,j) 點的判斷
                if (j === 0) {
                    T[i][j] = T[i - 1][j]   // 上一列
                } else {
                    if (s1[i] == s2[j]) {
                        T[i][j] = T[i - 1][j - 1] + 1
                    } else {
                        T[i][j] = Math.max(T[i - 1][j], T[i][j - 1])
                    }
                }
            }
        }
    }

    console.log(T)

    // ==== step2，反向查找，尋找最佳組合 ====
    result = []
    i = s1.length - 1
    j = s2.length - 1

    while (i > 0 && j > 0) {
        if (s1[i] == s2[j]) {
            result.unshift(s1[i]);
            i--;
            j--;
        } else {
            //向左或向上回退
            if (T[i - 1][j] > T[i][j - 1]) {
                //向上回退
                i--;
            } else {
                //向左回退
                j--;
            }
        }
    }

    // 加入起始點
    if (s1[0] == s2[0]) {
        result.unshift(s1[0])
    }

    console.log(result)
}

findMaxCommonString("acbad", "abcadf")  // abad
findMaxCommonString("fish", "hish")     // ish
findMaxCommonString("fosh", "fish")     // fsh
findMaxCommonString("fosh", "fort")     // fo