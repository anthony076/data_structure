
function findMaxMine(mines, totalWorker) {
    let golds = [], workers = [], T = [], result = [];

    // ==== step1，get golds and works from input-mines ====
    for (var mine of mines) {
        golds.push(mine.gold)
        workers.push(mine.worker)
    }

    // ==== step2，create table ====
    for (var i = 0; i < golds.length; i++) {
        T[i] = [];

        for (var j = 0; j <= totalWorker; j++) {
            if (i === 0) {
                // 第一列，只有一座礦
                if (j < workers[i]) {
                    // 只有一座礦，且人數不足時，無法採礦，因此最大金礦量為0
                    T[i][j] = 0
                } else {
                    // 只有一座礦，且超過需求人數時，
                    // 所有礦都被採完了，再多的人也沒用，因次最大採礦量為當前礦區的礦量
                    T[i][j] = golds[i]
                }
            } else {
                // 其他列，有多座礦
                if (j < workers[i]) {
                    // 當前礦場不可用，狀況等同於前一個礦場
                    T[i][j] = T[i - 1][j]
                } else {
                    // 當前礦場可用，
                    // 扣除當前礦區的量(golds[i])後，尋找剩餘人數(j-workers[i])在上一個礦區[i-1]的最大金礦量
                    T[i][j] = golds[i] + T[i - 1][j - workers[i]]
                }
            }
        }
    }

    console.log("Table:")
    console.log(T)

    // ==== step3，反向查表，尋找最大金礦量的最佳礦場組合 ====
    i = Mines.length - 1, j = totalWorker, remindGold = T[i][j];
    console.log(`Max Gold : ${remindGold}`)

    while (remindGold != 0) {
        if (T[i][j] < golds[i]) {
            i--
        } else {
            result.push(Mines[i])
            remindGold -= golds[i]
            i--
        }
    }

    console.log(result)
}

Mines = [
    { gold: 200, worker: 3 },
    { gold: 300, worker: 4 },
    { gold: 350, worker: 3 },
    { gold: 400, worker: 5 },
    { gold: 500, worker: 5 },
]

/* Mines = [
    { gold: 200, worker: 3 },
    { gold: 300, worker: 4 },
    { gold: 350, worker: 3 },
] */

findMaxMine(Mines, 10)













123










123
