
// 因為查表時只需要上一橫列中的其中一個解，
// 因此，可以只儲存上一橫列的結果，來對空間進行優化
function findMaxMine(mines, totalWorker) {
    let golds = [], workers = [], preResult = [], curResult = [];

    // ==== step1，get golds and works from input-mines ====
    for (var mine of mines) {
        golds.push(mine.gold)
        workers.push(mine.worker)
    }

    // ==== step2，create table ====
    // 2-1，初始化 preResult (preResult == 第一列)
    for (var j = 0; j <= totalWorker; j++) {
        if (j < workers[0]) {
            preResult[j] = 0
        } else {
            preResult[j] = golds[0]
        }
    }

    // 2-2，利用 preResult 查找之前的結果，
    // 注意，不要讓 preResult 和 curResult 產生引用
    for (var i = 1; i < golds.length; i++) {
        for (var j = 0; j <= totalWorker; j++) {
            if (j < workers[i]) {
                curResult[j] = preResult[j]
            } else {
                curResult[j] = golds[i] + preResult[j - workers[i]]
            }
        }

        // curResul 計算完畢後，再將當前的結果儲存在 preResult
        // preResult = curResult，錯誤用法，此行會建立引用，讓 preResult 和 curResult 造成連動
        preResult = curResult.slice(0)  // 正確用法，避免 preResult 和 curResult 產生連動
    }

    console.log(preResult)
    let MaxGold = preResult[preResult.length - 1]

    // ==== step3，查找組合 ====
    let result = [];

    for (var i = golds.length - 1; i >= 0; i--) {
        if (MaxGold >= golds[i]) {
            result.push(mines[i]);
            MaxGold -= golds[i]
        }
    }

    console.log(result)
}

/* Mines = [
    { gold: 200, worker: 3 },
    { gold: 300, worker: 4 },
    { gold: 350, worker: 3 },
    { gold: 400, worker: 5 },
    { gold: 500, worker: 5 },
] */

Mines = [
    { gold: 200, worker: 3 },
    { gold: 300, worker: 4 },
    { gold: 350, worker: 3 },
]

findMaxMine(Mines, 10)
