
function item2arr(items, valueArr, weightArr) {
    for (var item of items) {
        valueArr.push(item.value)
        weightArr.push(item.weight)
    }
}

function FindMaxValue(items, capacity) {
    let T = [], itemLength = items.length, result = [];

    // ==== step1，get values and weights from items ====
    let valueArr = [], weightArr = [];
    item2arr(items, valueArr, weightArr);

    // ==== step2，create table ====
    for (var i = 0; i < itemLength; i++) {
        T[i] = [];

        for (var j = 0; j <= capacity; j++) {

            //  case 2-1 容量為0，無法裝入任何物品，因此 最大價值必為0
            if (j === 0) {
                T[i][j] = 0;
            }

            // case 2-2 j < weightArr[i]，代表當前物品無法使用
            //      若 i=0，沒有其他物品，T[i][j] = 0
            //      若 i!=0，有其他物品，將其他物品裝入背包中
            if (j < weightArr[i]) {
                if (i === 0) {
                    T[i][j] = 0
                } else {
                    T[i][j] = T[i - 1][j]
                }
            }

            // case 2-3 j >= weightArr[i]，代表當前物品可以使用
            //      若 i=0，沒有其他物品，T[i][j] = valueArr[i]
            //      若 i!=0，有其他物品，選擇最大價值的組合
            if (j >= weightArr[i]) {
                if (i === 0) {
                    T[i][j] = valueArr[i]
                } else {
                    T[i][j] = Math.max(valueArr[i] + T[i - 1][j - weightArr[i]], T[i - 1][j]);
                }
            }
        }
    }

    console.log(T)

    // ==== find item from table ====
    // 從表格的右下角開始尋找
    i = items.length - 1;
    j = capacity

    while (i > 0 && j > 0) {
        console.log(`i:${i}, j:${j}`)

        // 與頭頂比較，若價值不相同，表示當前物品有被使用，
        // 紀錄該物品後，i移動到上一個物品，j移動到剩餘容量
        if (T[i][j] !== T[i - 1][j]) {
            console.log(`select item:${i}, value:${valueArr[i]}, weight:${weightArr[i]}`)
            result.push(i)
            j = j - weightArr[i]
            i--
        } else {
            // 與頭頂比較，若價值相同，表示當前物品未被使用，
            // 該物品不列入紀錄，i移動到上一個物品
            i--
        }

        // 若 i == 0，且 T[i][j] !=0，取用最小物品
        if (i === 0 && T[i][j] != 0) {
            console.log(`select item:${i}, value:${valueArr[i]}, weight:${weightArr[i]}`)
            result.push(i)
        }
    }

    console.log(result)
}

items = [
    { value: 3, weight: 2 },
    { value: 4, weight: 3 },
    { value: 5, weight: 4 },
]

FindMaxValue(items, 6)