
function findMax(left, right) {
    if (left < right) {
        return right
    } else {
        return left
    }
}

// 由下至上的累進法，會將最大和浮到金字坦頂端
// 若是由上至下的累進法，需要在填表完成後，針對底部列進行排序，
// 因為最大值在底部列，需要排序後才能將最大值取出
function minSumOfPyramid(arr) {
    let layer = arr.length

    // i=層數，i=layer-2，從底部出發
    for (var i = layer - 2; i >= 0; i--) {
        for (var j = 0; j <= i; j++) {
            // 注意，計算累積值的時候會去修改原始 arr 的內容
            // 此處的 left 和 right 不是原始值，而是累積值
            left = arr[i + 1][j];       //下層且同欄的元素
            right = arr[i + 1][j + 1]   //下層且下一欄的元素
            max = findMax(left, right)

            // 當前累進值 = 當前元素值 + 上一層的較大累進值
            arr[i][j] += max;
        }
    }
    console.log(arr[0][0])
}

/* arr = [
    [1],
    [2, 1],
    [1, 2, 9],
    [1, 2, 8, 9],
    [3, 3, 2, 9, 9]
]; */

arr = [
    [1],
    [2, 3],
    [4, 5, 6],
    [7, 8, 9, 10],
];

minSumOfPyramid(arr)
