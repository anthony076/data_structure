
function findMax(left, right) {
    if (left < right) {
        return right
    } else {
        return left
    }
}

function minSumOfPyramid_optimize(arr) {
    // 對空間的優化，利用 1d-arr 儲存每次累積的結果，避免直接去修改原始的 arr

    // accArr，儲存每次累積的結果
    let accArr = []
    let layer = arr.length  // 4

    for (var i = layer - 1; i >= 0; i--) {  // i=3,2,1,0，指向層數
        for (var j = 0; j <= i; j++) {      // j=0,1,2,3..i，指向第n個元素
            if (i == layer - 1) {
                // 最下層時不累進，只存放元素
                accArr[j] = arr[i][j]

            } else {
                left = accArr[j];
                right = accArr[j + 1]
                max = findMax(left, right)

                // 當前累進值 = 當前元素值 + 上一層的較大累進值
                // 把新累積值在 accArr 中直接覆蓋，舊的累積值沒有作用
                accArr[j] = arr[i][j] + max
            }
        }
    }

    console.log(accArr[0])
}

arr = [
    [1],
    [2, 3],
    [4, 5, 6],
    [7, 8, 9, 10],
];

minSumOfPyramid(arr)
