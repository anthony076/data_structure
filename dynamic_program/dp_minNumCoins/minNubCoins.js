
function minCoins(coins, target, coinLength) {
    var T = [];

    // step1，DP 之建立表格
    for (let i = 0; i < coinLength; i++) {
        T[i] = []
        for (let j = 0; j <= target; j++) {
            if (j == 0) {
                T[i][j] = 0;
                continue;
            }

            if (i == 0) {
                // i == 0，當前面額為最小面額 1元
                // 注意，面額一定要有最小面額1，否則會無解
                T[i][j] = j
            } else {
                if (j >= coins[i]) {
                    T[i][j] = 1 + T[i][j - coins[i]]
                } else {
                    T[i][j] = T[i - 1][j];
                }
            }
        }
    }

    console.log(T)

    // step2，利用表格 T，尋找組成 target 的最少硬幣組合
    findValue(coins, target, coinLength, T);
}

function findValue(coins, target, coinLength, T) {
    var i = coinLength - 1, // 從最大面額開始
        j = target,
        s = [];             // 最後結果

    // 先判斷起始位址是否需要先往上移動
    // 先往上移動的條件 T[i][j] == T[i - 1][j]，代表當前面額(i)無法組成當前總額(j)
    while (i > 0 && j > 0) {
        if (T[i][j] != T[i - 1][j]) {
            break
        } else {
            i--;
        }
    }

    while (i >= 0 && j > 0) {
        s.push(coins[i]);
        j = j - coins[i];

        if (j == 0) {
            break; //计算结束，退出循环
        }

        //如果 i == 0,那么就在第 0 行一直循环计算，直到 j=0即可
        if (i > 0) {
            while (T[i][j] == T[i - 1][j]) {
                i--;
                if (i == 0) {
                    break;
                }
            }
        }
    }

    console.log(s);

}

minCoins([1, 2, 5, 6], 11, 4)
