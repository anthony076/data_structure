const Dataset = require("../../dataSet")

function insertSort(dataset) {
  arr = dataset.arr

  for (var i = 1; i < arr.length; i++) {
    console.log("============")

    j = i // j 指針遍歷的元素，為當前元素前方的所有元素
    curValue = arr[j] // 用來保存當前排序中的元素值
    console.log(`i:${i}, curValue:${curValue}`)

    // 將當前元素前方的所有元素，都與當前元素進行比較
    // 注意1, 在同一輪中，當前元素是固定不動的，但前方元素較小，則前方元素就後退一格
    // 注意2, 因為 j 是會移動的，不要使用index的方式取值 (arr[j])，而是使用 curValue 做比較
    //       因為 arr[j] 是會變動的，但 curValue 是固定的
    // 注意3，添加 j>0 是為了不出現 j[-1] = undefined 的出現，undefined 會增加時間的消耗
    while (j > 0 && arr[j - 1] > curValue) {
      // 在此迴圈中，前一位的值較大，因此將前一位往後移動到當前的 index 位置
      // 注意，此處只需要將前一位的內容往後移，不需要交換內容，
      //      因為交換需要至少三個步驟，因此不需要每次都交換內容，
      //      只需要在確定位置後再插入，耗時用法，dataset.swap(j, j - 1)
      arr[j] = arr[j - 1]
      console.log(`move arr[${j - 1}]:${arr[j - 1]} to index:${j}`)
      console.log("arr:" + arr)

      j--
    }

    // 前方已經沒有更小的值，代表已找到合適位置，進行插入
    arr[j] = curValue
    console.log(`insert curValue:${curValue} to index:${j}`)
    console.log("arr:" + arr)
  }
}

function insertSort2(dataset) {
  arr = dataset.arr
  result = []

  for (var i = 0; i < arr.length; i++) {
    curValue = arr[i]
    if (curValue > arr[result.length - 1]) {
      result.push(curValue)
    } else {
      let j = result.length - 1

      while (curValue < result[j]) {
        --j
      }

      if (j < 0) {
        result.splice(0, 0, curValue)
      } else {
        result.splice(j, 0, curValue)
      }
    }
  }

  console.log(result)
}

d = new Dataset({ num: 0 })
d.setData([5, 4, 3, 2, 1])

// d = new Dataset({ min: 0, max: 50, num: 10 })

d.toString()
//insertSort(d)
insertSort2(d)
