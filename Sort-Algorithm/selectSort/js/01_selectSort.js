const Dataset = require("../../dataSet")

function findMinimum(arr) {
  let value = arr[0]

  for (var i = 0; i < arr.length; ++i) {
    if (arr[i] < value) {
      value = arr[i]
    }
  }

  index = arr.indexOf(value)

  return { index, value }
}

function selectSort(dataset) {
  dataset.toString()

  arr = dataset.arr
  let result = []

  let i = 0
  while (arr.length > 0) {
    // step1，find minimum item, and then remove it from arr
    minimum = findMinimum(arr)
    arr.splice(minimum.index, 1)

    // step2，get minimum value from current arr
    result[i] = minimum.value
    ++i
  }

  console.log(result)
}

function betterSelect(dataset) {
  dataset.toString()

  let minIndex;
  let arr = dataset.arr

  // 利用雙指針進行排序，
  //    i指針，移動指針，遍歷所有元素，找到最小值後 i+1
  //    j指針，遍歷 i 之後的所有元素，用來找最小值的 index
  //    若 j指針指向的值較小，就與 i 指針的內容交換
  for (var i = 0; i < arr.length - 1; ++i) {
    // ==== find index of minimum ====
    minIndex = i
    for (var j = i; j < arr.length; ++j) {
      console.log(j)
      if (arr[minIndex] > arr[j]) {
        minIndex = j
      }
    }

    // ==== 判斷是否要交換 ====
    if (i !== minIndex) {
      dataset.swap(i, minIndex)
    }
  }

  console.log(dataset.toString())
}


d = new Dataset({ num: 5 })
d.setData([5, 4, 3, 2, 1])
//d = new Dataset({ min: 0, max: 50, num: 10 })

//selectSort(d)
betterSelect(d)
