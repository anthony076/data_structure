/*
 選擇排序 Select Sort

 概念，
    step1: 尋找陣列最小值的index，
    step2: 若最小值的index與當前位置的index不同，兩者互相交換
 需求，
    判斷陣列中的最小值，並返回其index
    交換陣列元素
 流程，
    陣列為[3,5,1,4,9,2]，目前位置 index=0，陣列[3,5,1,4,9,2]中，最小值=1，最小值index=2，0 =\=2，得到新陣列[1,5,3,4,9,2]
    陣列為[1,5,3,4,9,2]，目前位置 index=1，陣列[1,5,3,4,9,2]中，最小值=2，最小值index=5，1 =\=5，得到新陣列[1,2,3,4,9,5]
    陣列為[1,2,3,4,9,5]，目前位置 index=2，陣列[1,2,3,4,9,5]中，最小值=3，最小值index=2，2 ==2， 得到舊陣列[1,2,3,4,9,5]
    陣列為[1,2,3,4,9,5]，目前位置 index=3，陣列[1,2,3,4,9,5]中，最小值=4，最小值index=3，3 ==3， 得到舊陣列[1,2,3,4,9,5]
    陣列為[1,2,3,4,9,5]，目前位置 index=4，陣列[1,2,3,4,9,5]中，最小值=5，最小值index=5，3 =\=5，得到新陣列[1,2,3,4,5,9]
 */

#include <iostream>

#define SWAP(x,y) {int t; t = x; x = y; y = t;}

using namespace std;

// 兩個元素相比較的選擇方式
// 例如，值3 值8，3-8 < 0
int ascending(int a, int b){
    return a - b;
}

// selectedIdx(原始array, 未比較的元素起始, 未比較的元素結尾, 比較方式)
int selectedIdx(int* arr, int from, int to, int(*compar)(int, int)) {
    // 將第一個元素(from) 設為初始selected
    int selected = from;

    // 將目前範圍中的每一個元素的都跟 selected 相比，selected = 兩值之間值小者的index
    for(int i = from + 1; i < to; i++){

        if(compar(arr[i], arr[selected]) < 0) {
            selected = i;
        }
    }

    // 每比較完一輪，返回最小值的 index
    return selected;
}

// select sort 演算法
void selectSort(int* arr, int len, int(*compar)(int,int)){
    for(int i=0;i<len;i++){

        // 返回該輪比較中，最小值的index
        int selected = selectedIdx(arr,i,len,compar);

        // i是目前被比較元素的 index
        if(selected != i)   // 代表目前的元素不是最小的
        {
            // 將最小元素與目前元素交換位置
            SWAP(arr[i], arr[selected])
        }
    }
}
