//
// Created by ChingHua Yang on 2018/5/31.
//

#ifndef SELECT_SORT_SELECT_SORT_H
#define SELECT_SORT_SELECT_SORT_H
int ascending(int a, int b);
int selectedIdx(int* arr, int from, int to, int(*compar)(int, int));
void selectSort(int* arr, int len, int(*compar)(int,int));
#endif //SELECT_SORT_SELECT_SORT_H
