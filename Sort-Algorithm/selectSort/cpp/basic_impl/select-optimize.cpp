#include <iostream>
using namespace std;

/*
陣列為[3,2,1]，i=0，j=0，arr[0]=3 vs arr[0]=3，3 = 3 不交換，陣列為[3,2,1]，自己跟自己比，浪費的比較
陣列為[3,2,1]，i=0，j=1，arr[0]=3 vs arr[1]=2，3 > 2 交換，  陣列為[2,3,1]
陣列為[2,3,1]，i=0，j=2，arr[0]=2 vs arr[2]=1，2 > 1 交換，  陣列為[1,3,2]
陣列為[1,3,2]，i=1，j=1，arr[1]=3 vs arr[1]=3，3 = 3 不交換，陣列為[1,3,2]，自己跟自己比，浪費的比較
陣列為[1,3,2]，i=1，j=2，arr[1]=3 vs arr[2]=2，3 > 2 交換，  陣列為[1,2,3]
陣列為[1,2,3]，i=2，j=2，arr[2]=3 vs arr[2]=3，3 = 3 不交換，陣列為[2,1,3]，自己跟自己比，浪費的比較
*/

void SelectSort(int *Arr,int nLength)
{
    int i = 0;
    for (i = 0; i < nLength; i++)
    {
        for (int j = i; j < nLength; j++)
        {
            if (Arr[i] > Arr[j])
            {
                // 互換元素內容
                Arr[j] = Arr[j]^Arr[i];
                Arr[i] = Arr[j]^Arr[i];
                Arr[j] = Arr[j]^Arr[i];
            }
        }
    }
}
