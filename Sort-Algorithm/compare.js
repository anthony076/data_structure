const Dataset = require("./dataset");

function bubbleSort(dataset) {
    arr = dataset.arr

    count = 0
    for (var i = 0; i < arr.length - 1; i++) {
        for (var j = 0; j < arr.length - 1 - i; j++) {
            if (arr[j] > arr[j + 1]) {
                dataset.swap(j, j + 1);
            }
            count++
        }
    }
}

function selectSort(dataset) {
    let arr = dataset.arr

    let minIndex;
    for (var i = 0; i < arr.length - 1; ++i) {
        minIndex = i
        for (var j = i; j < arr.length; ++j) {
            if (arr[minIndex] > arr[j]) {
                minIndex = j
            }
        }
        if (i !== minIndex) {
            dataset.swap(i, minIndex)
        }
    }
}

function insertSort(dataset) {
    arr = dataset.arr

    for (var i = 1; i < arr.length; i++) {
        j = i
        curValue = arr[j]

        while (j > 0 && arr[j - 1] > curValue) {
            arr[j] = arr[j - 1]
            j--
        }

        arr[j] = curValue
    }
}

function insertSort2(dataset) {
    arr = dataset.arr
    result = []

    for (var i = 0; i < arr.length; i++) {
        curValue = arr[i]
        if (curValue > arr[result.length - 1]) {
            result.push(curValue)
        } else {
            let j = result.length - 1

            while (curValue < result[j]) {
                --j
            }

            if (j < 0) {
                result.splice(0, 0, curValue)
            } else {
                result.splice(j, 0, curValue)
            }
        }
    }
}

function getData(arr) {
    if (!arr) {
        d = new Dataset({ min: 0, max: 1000000, num: 10000 })
    } else {
        d = new Dataset({})
        d.setData(arr)
    }
    return d
}

function run_compare(fn, dataset) {
    start = new Date().getTime()
    fn(dataset)
    stop = new Date().getTime()
    console.log(`times: ${stop - start}ms`)
}

function compare() {
    run_compare(bubbleSort, getData())
    run_compare(selectSort, getData())
    run_compare(insertSort, getData())
    run_compare(insertSort2, getData())
}

for (var i = 0; i < 10; ++i) {
    console.log("==============")
    compare()
}

