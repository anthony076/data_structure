const Dataset = require("../../../dataSet")

/* Bubble sort
   #1，`依序`遍歷每兩個元素，利用 swap 將小的排前，大的排後
   #2，第一輪需要遍歷 n-1個數，第2輪需要遍歷 n-1-1個數(因為第一輪已經確定一個最大數)
       可以寫為每輪需要遍歷 n-1-i 次，因此，最差情況共需要 1+2+...(n-1)
       注意，內層循環的次數是隨著 dataset 的個數而變化

       以5個隨機數為例，第一輪需要 n-1 = 5-1=4次，第二輪需要3次，第三輪需要2次，依次類推
       (5-1)+3+2+1 = (首項+末項)*項數/2 = (4+1)*4/2 = 10次

   #3，缺點，因為透過 for-loop 遍歷，即使排列已經完成，也仍然需要繼續遍歷
       造成時間上的浪費，沒有其他機制可以判斷是否能提早結束，因此排列速度最慢
*/

function bubbleSort(dataset) {
  // 時間複雜度，O(n^2)
  arr = dataset.arr
  console.log(`dataset: ${arr}`)

  count = 0
  for (var i = 0; i < arr.length - 1; i++) {
    for (var j = 0; j < arr.length - 1; j++) {
      // 小的值排在前面，大的值排在後面，按照 小 大 的方式排序
      if (arr[j] > arr[j + 1]) {
        dataset.swap(j, j + 1);
      }

      console.log(arr)
      count++
    }
  }

  console.log(count)
}

function betterBubbleSort(dataset) {
  // 時間複雜度，(1+n)*n/2 = (n+n^2)/2，約為 O(n^2)
  // 可以減少內層迴圈的次數，但減少的次數有限，時間複雜度仍然是 O(n^2)
  arr = dataset.arr
  console.log(`dataset: ${arr}`)

  count = 0
  for (var i = 0; i < arr.length - 1; i++) {
    // 透過 arr.length - 1 - i，每輪結束後都會產生一個已經排序完成的數
    // 下一輪循環時就不需要再重複遍歷
    for (var j = 0; j < arr.length - 1 - i; j++) {
      if (arr[j] > arr[j + 1]) {
        dataset.swap(j, j + 1);
      }

      console.log(arr)
      count++
    }
  }

  console.log(count)
}

d1 = new Dataset({ num: 0 })
d1.setData([5, 4, 3, 2, 1])
bubbleSort(d1)

d2 = new Dataset({ num: 0 })
d2.setData([5, 4, 3, 2, 1])
betterBubbleSort(d2)
