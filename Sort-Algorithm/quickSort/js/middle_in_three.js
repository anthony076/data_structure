const Dataset = require("../../dataSet")

function swap(arr, a, b) {
    let sum = arr[a] + arr[b]
    arr[a] = sum - arr[a]
    arr[b] = sum - arr[b]
}

function getMid(arr, left, right) {
    mid = Math.floor((right - left) / 2)

    if (arr[left] > arr[mid]) {
        swap(arr, left, mid)
    }

    if (arr[left] > arr[right]) {
        swap(arr, left, right)
    }

    if (arr[mid] > arr[right]) {
        swap(arr, mid, right)
    }

    console.log(arr)
}

const d = new Dataset()
d.toString()
getMid(d.arr, 0, d.arr.length - 1)