const Dataset = require("../../dataSet")

function swap(arr, i, j) {
    sum = arr[i] + arr[j]
    arr[i] = sum - arr[i]
    arr[j] = sum - arr[j]
}

// 進行分區排序
function partition(arr, left, right) {
    console.log(`==== start partition ====`)
    var pre = left - 1,
        cur = left,
        pivot = arr[right]   // 以尾元素作為標準值
    console.log(`pre=${pre}, cur=${cur}, pivot=arr[right]=${pivot}`)

    // 移動條件，cur指針超過當前數組的長度
    while (cur < right) {

        // cur指針找到目標元素，找到小於標準值的元素，
        // 將該元素添加到 pre 指針區 (將 arr[cur] 和 pre+1 的位置交換)
        if (arr[cur] < pivot) {
            console.log(`---- find arr[cur]=arr[${cur}]=${arr[cur]} smaller than pivot`)
            ++pre
            console.log(`pre++, pre:${pre}`)
            swap(arr, cur, pre)

            console.log("---- swap arr[cur] & arr[pre] ----")
            console.log(`swaped arr:${arr}`)
        }

        //cur指針未找到目標元素，cur指針繼續向右移動
        ++cur
        console.log("---- target not found ----")
        console.log(`cur++, cur:${cur}`)
    }

    // cur 指針已搜尋完畢，pre指針向右移動
    ++pre
    console.log("---- end search ----")
    console.log(`pre++, pre:${pre}`)

    // 搜尋結束，將 pre指針和 pivot 互換
    swap(arr, pre, right)
    console.log("---- swap arr[pre] & pivot ----")
    console.log(`swaped arr:${arr}`)
    console.log(`==== end partition ====`)
    console.log(`pivotPosition:${pre}`)

    // 返回 pivot 最後位置
    return pre
}

function quick(arr, left, right) {
    console.log(`arr:${arr}, left:${left}, right:${right}`)
    var pivotPosition;

    if (left < right) {
        pivotPosition = partition(arr, left, right)
        quick(arr, left, pivotPosition - 1)
        quick(arr, pivotPosition + 1, right)
    }
}

function digFill_quicksort(dataset) {
    arr = dataset.arr
    quick(arr, 0, arr.length - 1)
}

// ===============================
/* const d = new Dataset({ num: 0 })
d.setData([5, 4, 3, 2, 1]) */

const d = new Dataset({ min: 0, max: 100, num: 20 })
d.toString()

digFill_quicksort(d)
d.toString()