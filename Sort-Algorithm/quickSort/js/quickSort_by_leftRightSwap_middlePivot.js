const Dataset = require("../../dataSet")

function swap(arr, i, j) {
    sum = arr[i] + arr[j]
    arr[i] = sum - arr[i]
    arr[j] = sum - arr[j]
}

// 方法，左右指針交換法
function partition(arr, left, right) {
    console.log("================")

    // set pivot position
    var pivot = arr[Math.floor((left + right) / 2)]

    console.log(`pivot=${pivot}, leftIndex:${left}, rightIndex:${right}`)

    var leftIndex = left, rightIndex = right;

    // leftIndex 從頭部出發，rightIndex 從尾部出發，遍歷直到兩個指針交叉，
    // 因此，leftIndex > rightIndex 或 rightIndex < leftIndex 的時候停止
    // 可寫成 leftIndex <= rightIndex 的時候遍歷
    while (leftIndex <= rightIndex) {
        // ==== 當前元素不是目標元素 ====
        // 將 arr 區分為左大右小的 arr，左小右大的元素不是搜尋目標元素，
        // 因此左小，leftIndex < pivot 或 右大，rightIndex > pivot 時，
        // 移動指針，將指針往中心的未搜尋區移動
        console.log(`current index, left: ${leftIndex}, right: ${rightIndex}`)

        while (arr[leftIndex] < pivot) {
            leftIndex++
            console.log()
            console.log(`arr[leftIndex] < pivot, not target\nleftIndex++, leftIndex=${leftIndex}`)
        }

        while (arr[rightIndex] > pivot) {
            rightIndex--
            console.log()
            console.log(`arr[rightIndex] > pivot, not target\nrightIndex--, rightIndex==${rightIndex}`)
        }

        // ==== 當前元素是目標元素 ====
        if (leftIndex <= rightIndex) {
            console.log()
            console.log(`find target, leftIndex: ${leftIndex}, rightIndex: ${rightIndex}, swap`)
            swap(arr, leftIndex, rightIndex)
            console.log(`swaped arr:${arr}`)
            console.log()

            // 交換後，將指針往中心移動
            leftIndex++;
            rightIndex--;
        }
    }

    console.log(`leftIndex: ${leftIndex}, rightIndex: ${rightIndex}`)
    // 返回 pivot 最後的位置，下次分割時，不包含 pivot 的值
    return leftIndex
}

// 透過遞歸，重複進行左右指針交換法來進行分區
function quick(arr, left, right) {
    var pivotIndex;

    if (arr.length > 1) {
        // 進行分區
        pivotIndex = partition(arr, left, right)
        console.log(pivotIndex)

        // 對 0 ~ pivot(左邊的數組)，再重新進行排序
        if (left < pivotIndex - 1) {
            quick(arr, left, pivotIndex - 1)
        }

        // 對 pivot ~ right(右邊的數組)，再重新進行排序
        if (pivotIndex < right) {
            quick(arr, pivotIndex, right)
        }
    }
}
function swap_quickSort(dataset) {
    arr = dataset.arr
    quick(arr, 0, arr.length - 1)
}

// ===============================
/* const d = new Dataset({ num: 0 })
d.setData([5, 4, 3, 2, 1]) */

const d = new Dataset()
d.toString()
swap_quickSort(d)
d.toString()