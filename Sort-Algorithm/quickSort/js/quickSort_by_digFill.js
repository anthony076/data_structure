const Dataset = require("../../dataSet")

// 進行分區排序
function partition(arr, left, right) {
    console.log(`==== start partition ====`)

    var i = left,
        j = right,
        pivot = arr[i]  // 將 pivot 設置在頭部，並且由 i 指針指向 pivot，以形成第一個坑
    console.log(`pivot=${pivot}, i=${i}, j=${j}`)

    // 當 i 指針和 j 指針尚未交叉
    // i < j，進行挖坑填補
    // i == j，將 pivot 填入最後的坑
    // i > j，指針交叉，停止遍歷
    while (i < j) {
        // 尋找錯位的元素(左大右小)，因此 左小右大不是目標元素，繼續遍歷，指針往中心移動

        // ==== 移動右指針(j指針)，搜尋右小的目標元素，否則右指針往左移動 ====
        // i < j，限制 j 最多只能退到 i 的位置
        while (i < j && arr[j] > pivot) {
            --j
            console.log("---- moving j-pointer ---- ")
            console.log(`j:${j}`)
        }

        console.log(`find right-target, arr[j]=arr[${j}]=${arr[j]}`)

        // 將j指針指向的目標元素，填入坑中
        // j指針移動的時候，i指針指向坑的位置; i指針移動的時候，j指針指向坑的位置;
        if (i < j) {
            // 將 j指針找到的元素填入i指針指向的坑中，注意，填入後，j指針成為新的坑
            arr[i] = arr[j]
            // i指針的坑被填滿後，將i指針往中心(往右)移動一格
            ++i
            console.log("----- inser right-target to hole -----")
            console.log(`current arr:${arr}`)
        }

        // ==== 移動左指針(i指針)，搜尋左大的目標元素，否則左指針往右移動 ====
        while (i < j && arr[i] < pivot) {
            ++i
            console.log("---- moving i-pointer ---- ")
            console.log(`i:${i}`)
        }

        console.log(`find left-target, arr[i]=arr[${i}]=${arr[i]}`)

        if (i < j) {
            arr[j] = arr[i]
            --j
            console.log("----- inser left-target to hole -----")
            console.log(`current arr:${arr}`)
        }
    }

    // 遍歷結束，begin == end，將pivot的值，填入最後的坑空

    arr[i] = pivot
    console.log(`---- insert pivot ----`)
    console.log(`current arr:${arr}`)
    console.log("==== end partition ====")
    console.log(`pivotPosition:${i}`)

    // i 是 pivot 最後插入的位置，利用 pivot 的位置進行分割
    return i
}

function quick(arr, left, right) {
    console.log(`arr:${arr}, left:${left}, right:${right}`)
    var pivotPosition;

    // 遞歸停止條件，left 是數組起始點，right 是數組結束點，
    // left == right，沒有元素
    // left > right，無效邊界
    // 因此 只有 left < right 的時候才進行遞歸
    if (left < right) {
        pivotPosition = partition(arr, left, right)
        // 處理pivot左側的子序列，
        // 不包含pivot，所以只到 pivotPosition-1
        quick(arr, left, pivotPosition - 1)
        // 處理右側子序列
        // 不包含pivot，所以從 pivotPosition+1 開始
        quick(arr, pivotPosition + 1, right)
    }
}

function digFill_quicksort(dataset) {
    arr = dataset.arr
    quick(arr, 0, arr.length - 1)
}

// ===============================
const d = new Dataset({ num: 0 })
d.setData([5, 4, 3, 2, 1])

//const d = new Dataset({ min: 0, max: 100, num: 50 })
d.toString()

digFill_quicksort(d)
d.toString()
