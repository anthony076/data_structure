const Dataset = require("../../dataSet")

function swap(arr, i, j) {
    sum = arr[i] + arr[j]
    arr[i] = sum - arr[i]
    arr[j] = sum - arr[j]
}

/**
 * 將比Pivot小的數值右移，比Pivot大的數值左移
 * 最後回傳Pivot的位置
 */
var partition = function (arr, left, right) {
    var i = left - 1;
    for (var j = left; j < right; j++) {
        if (arr[j] < arr[right]) {   // arr[right]為Pivot
            i++;                     // 計數有幾個比Pivot小的數值
            swap(arr, i, j);
        }
    }
    swap(arr, i + 1, right);          // 將Pivot移到中間
    return i + 1;
};

var quickSort = function (arr, left, right) {
    if (left < right) {
        // 取得前一輪排序, pivot 的最後位置
        var pivotLocation = partition(arr, left, right);
        // 對左側的子序列繼續進行 quicksort
        quickSort(arr, left, pivotLocation - 1);
        // 對右側的子序列繼續進行 quicksort
        quickSort(arr, pivotLocation + 1, right);
    }
};

function swap_quickSort(dataset) {
    arr = dataset.arr
    quickSort(arr, 0, arr.length - 1)
}

// ===============================
/* const d = new Dataset({ num: 0 })
d.setData([5, 4, 3, 2, 1]) */

const d = new Dataset()
swap_quickSort(d)
d.toString()


















wef



few
afwf


123