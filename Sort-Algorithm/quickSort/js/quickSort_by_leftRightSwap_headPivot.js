const Dataset = require("../../dataSet")

function swap(arr, i, j) {
    sum = arr[i] + arr[j]
    arr[i] = sum - arr[i]
    arr[j] = sum - arr[j]
}

//
function quick(arr, left, right) {
    if (left < right) {
        var i = left, j = right + 1;

        // arr[left]為Pivot，left相當於最左邊第一個元素
        pivot = arr[left]

        // 遍歷i, j，
        // 利用j 尋找右側錯位的元素(右小)
        while (true) {

            // 利用i 從 left->right 尋找錯位的元素(左大)，
            // arr[i] < pivot 時移動，arr[i] > pivot 時停止
            // 注意，arr[++i]，將 i+1後，再傳遞給 arr[]
            while (i + 1 < arr.length && arr[++i] < pivot);

            // 利用j 從 right->left 尋找錯位的元素(右小)，
            // arr[j] > pivot 時移動，arr[i]< pivot 時停止
            // 注意，arr[--i]，將 i-1後，再傳遞給 arr[]
            while (j - 1 > -1 && arr[--j] > pivot);

            // 判斷是否需要停止外層遍歷，
            // 若i,j的位置交叉
            // 代表範圍內，Pivot右邊已無比Pivot小的數值
            // 代表範圍內，Pivot左邊已無比Pivot大的數值
            if (i >= j)
                break;

            // 將比Pivot大的數值換到右邊，比Pivot小的數值換到左邊
            swap(arr, i, j);
        }

        swap(arr, left, j);    // 將Pivot移到中間
        quick(arr, left, j - 1);    // 對左子串列進行快速排序
        quick(arr, j + 1, right);   // 對右子串列進行快速排序
    }
};

function swap_quickSort(dataset) {
    arr = dataset.arr
    quick(arr, 0, arr.length - 1)
}

// ===============================
/* const d = new Dataset({ num: 0 })
d.setData([5, 4, 3, 2, 1]) */

const d = new Dataset()
swap_quickSort(d)
d.toString()
