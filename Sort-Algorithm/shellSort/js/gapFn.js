
// 注意，不合適的 gapList 有可能會造成排序結果錯誤

// method1，手動輸入 gapList
function manuGap(arr) {
    return arr
}

// method2，動態定義間格序列
function dynamicGap(arr) {
    let gap = 1
    let gapList = []

    while (gap < arr.length / 3) {
        gap = gap * 3 + 1
    }

    for (gap; gap > 0; gap = Math.floor(gap / 3)) {
        gapList.push(gap)
    }

    return gapList
}

// method3，二分法
function binaryGap(arr) {
    let gapList = []
    let gap = Math.floor(arr.length / 2)

    while (gap >= 1) {
        gapList.push(gap)
        gap = Math.floor(gap / 2)
    }

    return gapList
}

module.exports = { manuGap, dynamicGap, binaryGap }