const Dataset = require("../../dataSet")
const { manuGap, dynamicGap, binaryGap } = require("./gapFn")

function shellSort(dataset) {
    arr = dataset.arr

    // ==== 選擇 gap 產生的策略 ====
    //let gapList = dynamicGap(arr)
    //let gapList = manuGap([5, 2, 1])
    let gapList = binaryGap(arr)

    // 遍歷 gap 值
    for (gap of gapList) {
        console.log()
        console.log(`@gap=${gap}`)
        console.log("================")

        // i-loop，用來產生新的排序序列，會往 index 增加的方向往後移動，
        // 指針i 指向下一個排序序列的最後一個目標元素A，目標元素A = arr[i] = arr[gap]
        for (var i = gap; i < arr.length; i++) {
            console.log(`i=${i}\ntargetEl=arr[i]=arr[${i}]=${arr[i]}`)
            console.log(`j=i-gap=${i - gap}\ncompareEl=arr[j]=arr[${i - gap}]=${arr[i - gap]}`)

            if (arr[i - gap] > arr[i]) {
                console.log("arr[j] > arr[i], compareEl > targetEl, need swap")
            } else {
                console.log("arr[j] < arr[i], compareEl < targetEl, no need swap")
            }

            // 利用 gap 取得目標元素A，目標元素A = arr[gap]，以 gap 值當作目標元素的索引值
            targetEl = arr[i]

            //0 j迴圈用來在當前排序序列中移動，並進行插排排序，
            //  j 指針指向被比較的元素，且與目標元素A距離 gap，j = i-gap
            //1 進行插排排序時，上一個元素與當前元素距離 gap，因此 j-= gap
            for (var j = i - gap; j >= 0 && arr[j] > targetEl; j -= gap) {
                console.log()
                console.log(`  targetEl: arr[i] = arr[${i}] = ${targetEl}`)
                console.log(`  compareEl: arr[j] = arr[${j}] = ${arr[j]}`)

                // 插排排序，將小的數往前移動，大的數往後移動，
                // 類似插排的概念，大的數往後排，小的數就停止，並將元素放在最後位置
                arr[j + gap] = arr[j]
                console.log(`  swaped arr: ${arr}`)
            }

            // 插排排序，經過 j-loop 後，目標元素已經在合適的位置
            arr[j + gap] = targetEl
            console.log(`after j-loop, arr:${arr}`)
            console.log("---------------------------")

        }
    }
    console.log(arr)
}

//let d = new Dataset({ min: 0, max: 100, num: 5 })
let d = new Dataset()
d.setData([27, 14, 77, 16, 21])
d.toString()

shellSort(d)