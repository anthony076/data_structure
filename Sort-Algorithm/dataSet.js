
module.exports = function Dataset(params) {
    self = this;

    // for user not provide params
    if (!params) {
        params = {}
    }

    // for user not provide correct params
    let num = params.num ? params.num : 5
    let min = params.min ? params.min : 0
    let max = params.max ? params.max : 100

    self.arr = []
    self.pos = 0

    // generate random numbers
    initDataset = function (arr) {
        if (num <= 0) {
            return arr
        }

        for (var i = 0; i < num; ++i) {
            random_int = Math.floor(Math.random() * max) + min

            // ignore repeat number
            while (arr.includes(random_int)) {
                random_int = Math.floor(Math.random() * max) + min
            }

            arr.push(random_int)
        }
    }

    initDataset(self.arr)

    self.len = function () {
        return self.arr.length
    }

    self.setData = function (arr) {
        self.arr = arr
    }

    self.insert = function (item) {
        // 先 push ，再將 self.pos++
        self.arr.push(self.pos++)
    }

    self.swap = function (index1, index2) {
        let value1 = self.arr[index1]
        // 將 index2 位置的值，賦值到 index1 的位置
        self.arr[index1] = self.arr[index2]
        // 將 index1 位置的值(value1)，賦值到 index2 的位置
        self.arr[index2] = value1
    }

    self.toString = function () {
        if (self.arr.length > 0) {
            result = self.arr.join(" ")
            console.log(result)
        } else {
            console.log("{ }")
        }
    }

    self.clear = function () {
        self.arr = []
    }

}
