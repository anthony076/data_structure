const Dataset = require("../../dataSet")

// 取得arr中最大值的位元數
function getMaxDigital(arr) {
    // step1，尋找最大值
    max = -99999
    for (value of arr) {
        if (value > max) {
            max = value
        }
    }

    // step2，計算最大值的位元數
    let num = max;
    let count = 1;

    while (Math.floor(num / 10) > 0) {
        num = Math.floor(num / 10)
        count++
    }

    return count
}

// 進行分桶 + 取得分桶後的結果
function classify(arr, digital) {

    // ==== 建立空桶 ====
    var buckets = new Array(10)
    for (var i = 0; i < buckets.length; i++) {
        buckets[i] = []
    }

    // ==== 將元素放入桶中 ====
    // 遍歷所有元素
    for (var value of arr) {

        // 將字面量的位元數補齊，例如 "5" -> "0005"
        // 若沒有補齊，在高位原分桶的時候會放錯位置
        // 例如，
        strNum = value.toString()
        while (strNum.length < maxDigital) {
            strNum = "0" + strNum
        }

        index = strNum[digital]

        // 根據當前位元的值，取得桶子的編號，並將元素推入桶中
        // 例如，strNum="1234"，digital=1，則 index=strNum[digital]=2
        buckets[strNum[digital]].push(value)
    }

    // ==== 利用桶子粗略排序後，重新將元素從桶子中取出 ====
    var result = []
    for (bucket of buckets) {
        if (bucket.length != 0) {
            for (item of bucket) {
                result.push(item)
            }
        }
    }

    return result
}

// 基數排序
function radinSort(dataSet) {
    arr = dataSet.arr

    // 取得 arr 中，最大值的位元數，
    // 例如 Max=1234，返回4
    maxDigital = getMaxDigital(arr)

    // 對不同的位置進行多次分桶
    for (var digital = maxDigital - 1; digital >= 0; digital--) {
        arr = classify(arr, digital, maxDigital)
    }

    console.log(arr)
}

const d = new Dataset({ min: 0, max: 100, num: 10 })
d.toString()

radinSort(d)

