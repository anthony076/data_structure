
function mergeSort(arr) {
    var step = 1;
    var left, right;

    if (arr.length < 2) {
        return;
    }

    while (step < arr.length) {
        left = 0;
        right = step;

        while (right + step <= arr.length) {
            // signature，mergeArrays(arr, startLeft, stopLeft, startRight, stopRight)
            mergeArrays(arr, left, left + step, right, right + step);

            // 將 left指針向右移動 step 的長度
            left = right + step;

            // 將 right指針向右移動 step 的長度
            right = left + step;
        }

        if (right < arr.length) {
            mergeArrays(arr, left, left + step, right, arr.length);
        }

        // step = 1,2,4,8
        step *= 2;
    }
}

// 合併array
function mergeArrays(arr, startLeft, stopLeft, startRight, stopRight) {
    //step1 根據 startLeft/stopLeft/startRight/stopRight, 從 arr 複製出新的 left-array 和 right-array
    //step2 排序合併，比較 left-array/right-array，將每次排序的結果，替換 arr 對應的位置
    //      例如，arr = [5,1,3,4,2], left = [5,1], right = [3,4],
    //      arr:[5,1,3,4,2] -> [1,1,3,4,2] -> [1,3,3,4,2] -> [1,3,4,4,2] -> [1,3,4,5,2]

    console.log("==============")
    // ==== step1，建立 left-array/right-array ====
    // 建立 empty left-array/right-array
    var rightArr = new Array(stopRight - startRight + 1);
    var leftArr = new Array(stopLeft - startLeft + 1);

    // 根據 startLeft/stopLeft 將 arr 中的元素放進 empty left-array
    k = startLeft;
    for (var i = 0; i < (leftArr.length - 1); ++i) {
        leftArr[i] = arr[k];
        ++k;
    }
    console.log(`startLeft:${startLeft}, stopLeft:${stopLeft}`)

    // 根據 startRight/stopRight 將arr 中的元素放進 empty right-array
    var k = startRight;
    for (var i = 0; i < (rightArr.length - 1); ++i) {
        rightArr[i] = arr[k];
        ++k;
    }
    console.log(`startRight:${startRight}, stopRight:${stopRight}`)

    // 補上哨兵值，避免比較排序合併時，比較到 undefined 的值造成錯誤
    // 因為在排序比較的時候，k 移動的次數 = rightArr元素數量 + leftArr元素數量
    // 例如，[2], [1]，k 移動次數為 2, 則
    //      k=0時，Left[leftIndex=0]=2 vs Right[right=0]=1, 取最小值1, right++
    //      k=1時，Left[leftIndex=0]=2 vs Right[right=1]=1, 取最小值2,
    //
    //      若沒有補上哨兵值，Right[1] 就會錯誤，因為無法比較 undefined 的值
    //      1 > undefined，返回 false,  1 < undefined，返回 false
    //      若比較到 undefined 的值，就會插入錯誤的值
    //
    // 因此，rightArr元素數量 = k 移動次數，即 k=[1] -> k[1,inf]
    // 哨兵值是為了取出 leftArr 或 rightArr 中的最後一個剩餘元素
    rightArr[rightArr.length - 1] = Infinity; // 哨兵值
    leftArr[leftArr.length - 1] = Infinity; // 哨兵值

    // ==== step2 排序合併 ====
    var leftIndex = 0, rightIndex = 0;
    console.log()
    console.log(`leftArr:${leftArr}`);
    console.log(`rightArr:${rightArr}`);
    console.log(`before array:${arr}`)
    console.log()

    // k 移動的次數 = rightArr元素數量 + leftArr元素數量
    for (var k = startLeft; k < stopRight; ++k) {
        console.log(`k: ${k}`)

        if (leftArr[leftIndex] <= rightArr[rightIndex]) {
            console.log(`insert left:${leftArr[leftIndex]}`)
            arr[k] = leftArr[leftIndex];
            leftIndex++;
        }
        else {
            console.log(`insert right:${rightArr[rightIndex]}`)
            arr[k] = rightArr[rightIndex];
            rightIndex++;
        }
    }
    console.log(`after array: ${arr}`);
}

var nums = [6, 10, 1, 9, 4, 8, 2, 7, 3, 5];
mergeSort(nums);
console.log(nums)

