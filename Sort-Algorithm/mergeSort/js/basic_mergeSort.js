const Dataset = require("../../dataSet")

// 合併 left-array 和 right-array
function sort_and_concat(left, right) {
    console.log("============")
    console.log(`left:${left}, right:${right}`)

    let resultArray = [], leftIndex = 0, rightIndex = 0;

    // ==== 對 left/right array 進行排序 ====
    // 注意，此方式會剩餘一個元素未加入到 resultArray 中，
    //       此迴圈後，需要手動將剩餘的元素加入
    while (leftIndex < left.length && rightIndex < right.length) {
        console.log("  ------------")

        // 比較左右array 的 index ，比較小的就放入，並將 index+1
        if (left[leftIndex] < right[rightIndex]) {
            // left-array 中的元素較小，插入左邊元素
            resultArray.push(left[leftIndex]);
            leftIndex++;
        } else {
            // right-array 中的元素較小，插入右邊元素，
            resultArray.push(right[rightIndex]);
            rightIndex++;
        }
        console.log("  " + resultArray)
    }

    // ==== 將排序剩餘、未插入 resultArray 的元素，插入到 resultArray 中 ====
    // 方法一
    /* result = resultArray
        .concat(left.slice(leftIndex))
        .concat(right.slice(rightIndex)); */

    // 方法二
    while (leftIndex < left.length) {
        resultArray.push(left[leftIndex++]);
    }

    while (rightIndex < right.length) {
        resultArray.push(right[rightIndex++]);
    }

    console.log()
    console.log(`  resultArray:${resultArray}`)
    return resultArray
}

// 利用遞歸函數進行切割 array
function split(arr) {
    // ==== 只有一個元素的時候不需要進行排序，直接返回 ====
    // 注意，加入此判斷，才能限制遞歸分割的最小元素為1，
    //      當只剩下一個元素後，開始返回並合併
    if (arr.length <= 1) {
        return arr;
    }

    // ==== 分割 arr ====
    // 取得當前陣列長度/2 的整正數
    const middle = Math.ceil(arr.length / 2);

    //1 切分 arr 為 left-array 和 right-array，對半分割
    //2 若 arr = [1,2,3,4,5]，middle = 2，
    //  arr.slice(0, middle)，返回 0,1，從 0 開始，取兩位
    //  arr.slice(middle)，   返回 2,3,4，從index=2 ，取所有
    const left = arr.slice(0, middle);
    const right = arr.slice(middle);

    // ==== 排序與合併 arr ====
    result = sort_and_concat(split(left), split(right));

    return result
}

const d = new Dataset()
d.setData([8, 7, 6, 5, 4, 3, 2, 1])
split(d.arr)
