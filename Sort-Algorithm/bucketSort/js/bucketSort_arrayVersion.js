const Dataset = require("../../dataSet")

function myMinMax(arr, mode = "min") {
    var result;

    if (mode === "max") {
        result = -999999

        for (var num of arr) {
            if (num > result) {
                result = num
            }
        }
    } else {
        result = 999999

        for (var num of arr) {
            if (num < result) {
                result = num
            }
        }
    }

    return result
}

function bucketSort(dataset, capacity) {
    arr = dataset.arr
    min = myMinMax(arr, "min")
    max = myMinMax(arr, "max")

    // ==== step1，計算桶子數量 ====
    let bucketCount = Math.ceil((max - min + 1) / capacity);

    // ==== step2，建立分桶，bucket[0] 為第0桶 ====
    // 建立大桶
    let buckets = new Array(bucketCount);

    for (let i = 0; i < bucketCount; ++i) {
        buckets[i] = [];
    }

    // ==== step3，對數組中的元素進行分桶 ====
    for (var value of arr) {
        // 取得桶子編號(k)
        let k = Math.floor((value - min) / capacity);

        // 將當前值推入對應的桶子中
        buckets[k].push(value);
    }

    // ==== step4，對分桶中的元素進行排序，並取得結果 ====
    // 指向arr中的位置，用來覆蓋原始 arr 中的元素
    let p = 0;

    // 遍歷分桶
    for (let bucket of buckets) {

        // 對桶中的元素進行排序，注意，排序可換成其他方式
        bucket = bucket.sort((a, b) => a - b);  // 升序排序

        // 取得分桶中的數值，並寫入原始 arr 中
        for (let value of bucket) {
            arr[p] = value;
            p++
        }
    }

    console.log(arr)
}

//const d = new Dataset()
const d = new Dataset({ min: 0, max: 100, num: 20 })

bucketSort(d, 6)