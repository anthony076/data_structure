// from https://dailc.github.io/2016/12/03/baseKnowlenge_algorithm_sort_bucketSort.html
var L = require('linklist'); //链表

var sort = function (arr, bucketCount) {

    // arr 中元素<=1 不進行排序，直接返回
    if (arr.length <= 1) {
        return arr;
    }

    bucketCount = bucketCount || 10;
    //初始化桶
    var len = arr.length,
        buckets = [],
        result = [],
        max = arr[0],
        min = arr[0];

    // 尋找 arr 中的最大和最小值
    for (var i = 1; i < len; i++) {
        min = min <= arr[i] ? min : arr[i];
        max = max >= arr[i] ? max : arr[i];
    }

    //求出每一个桶的数值范围
    var space = (max - min + 1) / bucketCount;

    //将数值装入桶中
    for (var i = 0; i < len; i++) {
        //找到相应的桶序列
        var index = Math.floor((arr[i] - min) / space);

        //判断是否桶中已经有数值
        if (buckets[index]) {
            //数组从小到大排列
            var bucket = buckets[index];
            var insert = false; //插入标石

            L.reTraversal(bucket, function (item, done) {
                if (arr[i] <= item.v) { //小于，左边插入
                    L.append(item, _val(arr[i]));
                    insert = true;
                    done(); //退出遍历
                }
            });

            if (!insert) { //大于，右边插入
                L.append(bucket, _val(arr[i]));
            }
        } else {
            var bucket = L.init();
            L.append(bucket, _val(arr[i]));
            buckets[index] = bucket; //链表实现
        }
    }

    //开始合并数组
    for (var i = 0, j = 0; i < bucketCount; i++) {
        L.reTraversal(buckets[i], function (item) {
            // console.log(i+":"+item.v);
            result[j++] = item.v;
        });
    }
    return result;
};

//链表存储对象
function _val(v) {
    return {
        v: v
    }
}

//开始排序
arr = bucketSort(arr, self.bucketCount);