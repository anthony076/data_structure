
const Dataset = require("../../dataSet")

function myMinMax(arr, mode = "min") {
    var result;

    if (mode === "max") {
        result = -999999

        for (var num of arr) {
            if (num > result) {
                result = num
            }
        }
    } else {
        result = 999999

        for (var num of arr) {
            if (num < result) {
                result = num
            }
        }
    }

    return result
}

function countSort(dataset) {
    const arr = dataset.arr;

    if (arr.length <= 1) {
        return arr
    }

    // ==== step1，取得資訊 ====
    min = myMinMax(arr, "min")
    max = myMinMax(arr, "max")

    // ==== step2，建立輔助數組 ====
    const countArr = Array(max - min + 1).fill(0)

    // 統計出現次數
    for (num of arr) {
        countArr[num - min] += 1
    }

    // 計算累進值
    for (var i = 1; i < countArr.length; ++i) {
        countArr[i] = countArr[i] + countArr[i - 1]
    }

    // ==== step3，根據輔助數組進行排序 ====
    const resultArr = Array(arr.length)

    for (let i = arr.length - 1; i >= 0; i--) {
        // 注意，此處是遍歷 arr 中的元素，並利用從尾部插入
        // 利用 mapValue 在 countArr 表取得要填入的位置後，將位置往前移動 (countArr[mapValue] -= 1)
        // 此方法為穩定排序法，因為從尾部遍歷，亦從尾部插入

        // 利用 map 值尋找 countArr 中對應的位置
        mapValue = arr[i] - min
        writePosition = countArr[mapValue] - 1
        resultArr[writePosition] = arr[i]
        countArr[arr[i] - min] -= 1
    }

    console.log(resultArr)
}

const d = new Dataset({ num: 0 })
d.setData([12, 15, 13, 10, 12, 13, 10, 13])

//const d = new Dataset({ min: 0, max: 10, num: 5 })

d.toString()

countSort(d)

