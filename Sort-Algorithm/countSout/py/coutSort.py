def myMinMax(Arr, mode="min"):
    if mode == "max":
        result = -999999

        for num in Arr:
            if num > result:
                result = num
    else :
        result = 999999
        for num in Arr:
            if num < result:
                result = num

    return result


def counting_sort(collection):
    if collection == []:
        return []

    # ==== step1，取得資訊 ====
    arr_length = len(collection)
    coll_max = myMinMax(collection, mode="max")
    coll_min = myMinMax(collection, mode="min")

    counting_arr_length = coll_max + 1 - coll_min
    counting_arr = [0] * counting_arr_length

    # ==== step2，建立輔助數組 ====
    for number in collection:
        counting_arr[number - coll_min] += 1
    print("counting:{}".format(counting_arr))

    for i in range(1, counting_arr_length):
        counting_arr[i] = counting_arr[i] + counting_arr[i-1]
    print("accumulation:{}".format(counting_arr))

    # ==== step3，根據輔助數組進行排序 ====
    resultArr = [0] * arr_length

    # 從尾部開始填
    for i in range(arr_length-1, -1, -1):
        mapValue = collection[i] - coll_min
        resultArr[counting_arr[mapValue]-1] = collection[i]
        counting_arr[mapValue] -= 1

    return resultArr

print(counting_sort([12, 15, 13, 10, 12, 13, 10, 13]))
