const Dataset = require("../../dataSet")

function swap(arr, i, j) {
    sum = arr[i] + arr[j]
    arr[i] = sum - arr[i]
    arr[j] = sum - arr[j]
    return arr
}

// 最大堆樹
function MaxHeap(arr) {
    this.data = [];

    this.arr2heap = function () {
        // 從尾元素開始，依序對 array 中的元素做下濾更新
        index = this.data.length - 1
        for (var i = index; i >= 0; i--) {
            this.percolateDown(i, this.data.length)
        }
    }

    // 下濾更新 percolateDown()，往下更新 heap-tree，以符合最大堆的規則
    // 從指定的位置往下更新
    // 注意排序時，要排除已經排序好的元素，因此利用 size 來控制更新的範圍
    this.percolateDown = function (index, size) {
        while (true) {
            console.log("============")
            let leftIndex = 2 * index + 1,   // 左子節點位置
                rightIndex = 2 * index + 2,  // 右子節點位置
                largestIndex = index;        // 比較前，預設最大值的位置為當前位置
            console.log(`index:${index}, iLeft:${leftIndex}, iRight:${rightIndex}`)

            // ==== 尋找最大值的位置 ====
            // 左子節點值 > 最大值，將最大值的指標移動到左子節點的位置
            if (leftIndex < size && this.data[leftIndex] > this.data[largestIndex]) {
                largestIndex = leftIndex
            }

            // 右子節點值 > 當前值，將最大值的指標移動到右子節點的位置
            if (rightIndex < size && this.data[rightIndex] > this.data[largestIndex]) {
                largestIndex = rightIndex
            }

            // ==== 交換內容 ====
            // 將最大值的內容和當前位置的內容進行交換
            // 注意，指交換內容，指針的位置不變
            if (largestIndex !== index) {
                [this.data[largestIndex], this.data[index]] = [this.data[index], this.data[largestIndex]]
                index = largestIndex
            } else {
                break
            }
            console.log(`arr:${this.data}`)
        }
    }

    this.heapSort = function (dataset) {
        // ==== 將數組轉換為最大堆 ====
        this.data = dataset.arr
        this.arr2heap()

        // ==== 排序開始 ====
        // 從尾部依序遍歷元素
        console.log("==== start sorting ====")
        for (var i = this.data.length - 1; i > 0; i--) {
            // 從尾部開始，依序將頭元素的最大值，交換到當前的位置
            swap(this.data, 0, i)
            // 每交換一次位置後，就需要透過 percolateDown() 確保堆樹符合規則
            this.percolateDown(0, i)
        }

        console.log(this.data)
    }
}

const d = new Dataset({ num: 0 })
d.setData([12, 10, 69, 66, 87, 34, 99])
// const d = new Dataset({ min: 0, max: 100, num: 10 })

d.toString()

var heap = new MaxHeap();
heap.heapSort(d)
