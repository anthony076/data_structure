const Dataset = require("../../dataSet")

// 最大堆樹
function MaxHeap(arr) {
    this.data = [];

    // 上濾更新 percolate-up()，往上更新 heap-tree，以符合最大堆的規則
    // 從最底層往上更新
    this.percolateUp = function (index) {
        while (index > 0) {
            // 當前位置的值
            let curValue = this.data[index],
                // 父節點的位置
                parentIndex = Math.floor((index - 1) / 2),
                // 父節點的值
                parentValue = this.data[parentIndex];

            // ==== 檢查是否符合最大堆的規則 ====
            if (parentValue >= curValue) {
                // 符合最大堆規則，父節點最大，
                // 因此不用再繼續往上更新，跳出 while 迴圈
                break
            } else {
                // 不符合最大堆規則，進行值的交換
                this.data[index] = parentValue;
                this.data[parentIndex] = curValue;

                // 移動到下一個父節點
                index = parentIndex
            }
        }
    }

    // 下濾]更新 percolateDown()，往下更新 heap-tree，以符合最大堆的規則
    // 從指定的位置往下更新
    this.percolateDown = function (index) {
        let leftIndex = 2 * index + 1,   // 左子節點位置
            rightIndex = 2 * index + 2,  // 右子節點位置
            largestIndex = index;        // 比較前，預設最大值的位置為當前位置

        // ==== 尋找最大值的位置 ====
        // 當前位置尚未到達底部 且 (左子節點值 > 最大值)，將最大值的指標移動到左子節點的位置
        if (leftIndex <= this.data.length && this.data[leftIndex] > this.data[largestIndex]) {
            largestIndex = leftIndex
        }

        // 當前位置尚未到達底部 且 (右子節點值 > 當前值)，將最大值的指標移動到右子節點的位置
        if (rightIndex <= this.data.length && this.data[rightIndex] > this.data[largestIndex]) {
            largestIndex = rightIndex
        }

        // ==== 交換內容 ====
        // 將最大值的內容和當前位置的內容進行交換
        // 注意，指交換內容，指針的位置不變
        if (largestIndex !== index) {
            [this.data[largestIndex], this.data[index]] = [this.data[index], this.data[largestIndex]]

            // 將當前位置移動到最大值指向的位置
            this.percolateDown(largestIndex)
        }
    }

    // 將 value 插入到數組尾部，然後使用 percolateUp() 來向上更新
    this.insert = function (value) {
        // 將新增元素置於數組尾部
        this.data.push(value)
        // 執行上濾更新
        this.percolateUp(this.data.length - 1)
    }

    // 移除指定位置的值，然後使用 percolateDown() 來向上更新
    this.del = function (index) {
        // 將尾部元素移動到刪除位置
        this.data[index] = this.data[this.data.length - 1]
        // 將尾部元素刪除
        this.data.pop()

        let curValue = this.data[index],
            parentValue = this.data[Math.floor((index - 1) / 2)],
            leftValue = this.data[2 * index + 1],
            rightValue = this.data[2 * index + 2];

        // 最大堆的時候，刪除任意節點需要進行下濾更新操作，
        // 狀況一，尾元素移動到上層，把小數移動到上方，需要透過下濾更新，重新讓小數移動到正確位置
        // 狀況二，尾元素移動到同層，因為同層元素沒有順序關係，需要透過上濾更新，重新將尾元素移動到正確位置
        if (curValue < leftValue || curValue < rightValue) {
            this.percolateDown(index)           // For 刪除位置在尾部元素的上層
        } else if (curValue > parentValue) {
            this.percolateUp(index)             // For 刪除位置在尾部元素的同層
        }
    }

    this.extractMax = function () {
        max = this.data[0]
        this.del(0)

        return max
    }

    // 注意，必須透過 insert() 將元素放入堆樹中
    // insert() 才會透過 percolate() 檢查是否符合最大堆的規則，在必要時進行交換
    this.setData = function (dataset) {
        arr = dataset.arr

        // 入堆
        for (value of arr) {
            this.insert(value)
        }

        return this.data
    }

    this.clear = function () {
        this.data = []
    }

    this.heapSort = function () {
        var result = []
        len = this.data.length

        for (var i = 0; i < len; i++) {
            result.unshift(this.extractMax())
        }

        console.log(result)
    }
}

//const d = new Dataset()
const d = new Dataset({ num: 0 })
d.setData([12, 10, 69, 66, 87, 34, 99])
d.toString()

var heap = new MaxHeap();
heap.setData(d)
heap.heapSort()
