/*
    // 插入節點
    檢查是否為空， <----------- 判斷根節點
        若是，直接插入        |
        若否，               |
            決定方向 <--------
            判斷為空         | 遞歸
            移動 <-----------

    // 遍歷節點
    對於中序遍歷，可以想像拿著一個尺，由左側慢慢移動到右側的過程中，依序會遇到的節點
    對於前序遍歷，優先權先後，父節點 > 左節點 > 右節點 (父左右)
    對於後序遍歷，優先權先後，左節點 > 右節點 > 父節點 (左右父)

    // 刪除節點
    若是父節點的被刪除時，
        若父節點有單層子節點，則左側子節點會替代被刪除的父節點，原節點也會被刪除
        若父節點有子樹，則右側子樹中的最小節點會替代被刪除的父節點，原節點也會被刪除
    為什麼是`右側子樹中的最小節點`
        若沒有右側子樹，會自動變成左側子樹

*/

function Node(key) {
    this.key = key
    this.left = null
    this.right = null
}

function BinarySearchTree() {

    this.rootNode = null

    function insertNode(curNode, newNode) {
        // step1，先判斷要插入當前節點的左側或右側
        if (newNode.key < curNode.key) {
            // ==== 1-1 插入左側區 ====

            // step2，插入左側前，判斷左側節點是否為空
            if (curNode.left === null) {
                // 若是，執行新節點插入當前節點的左側
                curNode.left = newNode
            } else {
                // 若否，移動到下一個左側子節點，(將左側節點傳遞給 curNode 參數)
                insertNode(curNode.left, newNode)
            }

        } else if (newNode.key > curNode.key) {
            // ==== 1-2 插入右側區 ====

            // step2，插入右側前，判斷右側節點是否為空
            if (curNode.right === null) {
                // 若是，執行新節點插入當前節點的右側
                curNode.right = newNode
            } else {
                // 若否，移動到下一個右側子節點，(將左側節點傳遞給 curNode 參數)
                insertNode(curNode.right, newNode)
            }
        }
    }

    this.insert = (key) => {
        newNode = new Node(key)

        if (this.rootNode === null) {
            this.rootNode = newNode
        } else {
            insertNode(this.rootNode, newNode)
        }
    }

    // ==== 中序遍歷法 ====
    function inOrderNode(curNode) {
        if (curNode != null) {
            // 優先處理所有左節點
            inOrderNode(curNode.left)
            // 處理完左節點後，印出當前節點值
            console.log(curNode.key)
            // 最後處理所有右節點
            inOrderNode(curNode.right)
        }
    }

    this.inOrderIter = () => {
        // 帶入根節點後開始遞歸
        inOrderNode(this.rootNode)
    }

    // ==== 前序遍歷法 ====
    function rootFirstNode(curNode) {
        if (curNode != null) {
            //優先印出當前節點值
            console.log(curNode.key)
            //處理所有左節點
            rootFirstNode(curNode.left)
            //處理所有右節點
            rootFirstNode(curNode.right)
        }
    }

    this.rootFirstIter = () => {
        // 帶入根節點後開始遞歸
        rootFirstNode(this.rootNode)
    }

    // ==== 後序遍歷法 ====
    function leafFirstNode(curNode) {
        if (curNode != null) {
            //處理所有左節點
            leafFirstNode(curNode.left)
            //處理所有右節點
            leafFirstNode(curNode.right)
            // 印出當前節點值
            console.log(curNode.key)
        }
    }

    this.leafFirstIter = () => {
        // 帶入根節點後開始遞歸
        leafFirstNode(this.rootNode)
    }

    // ==== 尋找最小值 ====
    this.min = function () {
        if (this.rootNode == null) {
            return null
        } else {
            let curNode = this.rootNode

            while (curNode.left != null) {
                curNode = curNode.left
            }
            return curNode.key
        }
    }

    // ==== 尋找最大值 ====
    this.max = function () {
        if (this.rootNode == null) {
            return null
        } else {
            let curNode = this.rootNode

            while (curNode.right != null) {
                curNode = curNode.right
            }

            return curNode.key
        }
    }

    // ==== 尋找特定值，遞歸寫法 ====
    /* function searchKey(curNode, key) {
        if (curNode === null) {
            return false
        } else if (key < curNode.key) {
            return searchKey(curNode.left, key)
        } else if (key > curNode.key) {
            return searchKey(curNode.right, key)
        } else {
            return true
        }
    }

    this.has = function (key) {
        if (this.rootNode === null) {
            // tree 中沒有任何節點
            return false
        } else {
            return searchKey(this.rootNode, key)
        }
    } */

    // ==== 尋找特定值，不使用遞歸的寫法 ====
    // 尋找特定值的時候，不需要每個節點都找
    // 因為二元樹的特性，在比對值的時候，會自動往接近值靠近
    // 因此，只需要確定移動方向和判斷是否為 undefined 即可
    this.has = function (key) {
        if (this.rootNode == null) {
            return false
        } else {
            curNode = this.rootNode

            while (true) {
                // 沒有子節點的時候，curNode = curNode.left = undefined
                if (curNode == null) {
                    return false
                }

                // 根據當前節點的時，判斷移動的方向
                if (key < curNode.key) {
                    curNode = curNode.left
                } else if (key > curNode.key) {
                    curNode = curNode.right
                } else {
                    // key == curNode.key
                    return true
                }
            }
        }
    }

    this.getNode = function (key) {
        if (this.rootNode == null) {
            return false
        } else {
            curNode = this.rootNode

            while (true) {
                // 沒有子節點的時候，curNode = curNode.left = undefined
                if (curNode == null) {
                    return "node not found"
                }

                // 根據當前節點的時，判斷移動的方向
                if (key < curNode.key) {
                    curNode = curNode.left
                } else if (key > curNode.key) {
                    curNode = curNode.right
                } else {
                    // key == curNode.key
                    return curNode
                }
            }
        }
    }

    // ==== 刪除指定節點 ====
    // 同 this.min()，只是 this.getSmallestNode() 可以指定要搜尋的節點
    this.getSmallestNode = function (node) {
        if (node == null) {
            return null

        } else {
            while (node.left != null) {
                node = node.left
            }
            return node
        }
    }

    this.removeNode = function (curNode, key) {
        // ==== step1，判斷是否已經到達底部，到達底部後，就不需要再遍歷 ====
        if (curNode === null) {
            return null
        }

        if (curNode.key > key) {
            // == case1-1，當前節點`不是`目標節點，進行移動 + 節點更新，往左子節點移動 ==

            // 移動到下一個節點，並透過 this.removeNode() 的節點更新當前 node.left 的節點
            // this.removeNode(curNode.left, key)，移動到下一個左子節點
            curNode.left = this.removeNode(curNode.left, key)
            return curNode // 返回更新後的 node

        } else if (curNode.key < key) {
            // == case1-2，當前節點`不是`目標節點，進行移動 + 節點更新，往右子節點移動 ==

            // 移動到下一個節點，並透過 this.removeNode() 的節點更新當前 node.right 的節點
            // this.removeNode(curNode.right, key)，移動到下一個右子節點
            curNode.right = this.removeNode(curNode.right, key)
            return curNode // 返回更新後的 node

        } else {
            // == case1-3，當前節點是目標節點 ====

            // step2，找到目標節點後，判斷目標節點的類型，
            //      case2-1 無任何子節點，case2-2 只有右子節點，case2-3 只有左子節點，
            //      case2-4 子節點為一個子樹或雙子節點
            if (curNode.left == null & curNode.right == null) {
                // == case2-1 無任何子節點 ==
                return null
            } else if (curNode.left == null) {
                // == case2-2 只有右節點 ==
                // 返回右節點
                return curNode.right
            } else if (curNode.right == null) {
                // == case2-3 只有左節點 ==
                // 返回右節點
                return curNode.left
            } else {
                // == case2-4 子節點為一個子樹或雙子節點 ==
                // 將當前節點替換成右子樹中最小的節點，並將右子樹中最小的節點刪除

                // 2-4-1，尋找右子樹中最小的節點
                tempNode = this.getSmallestNode(curNode.right)

                // 2-4-2，將右子樹中最小的節點刪除，
                curNode.right = this.removeNode(curNode.right, tempNode.key)

                // 2-4-3，將當前節點的key 替換成右子樹中最小的節點
                curNode.key = tempNode.key

                // 2-4-4，返回變更後的節點
                return curNode
            }
        }
    }

    this.remove = function (key) {
        if (this.rootNode == null) {
            // 根節點為空，當前 tree 結構未添加任何節點
            return "no element found"
        } else {
            return this.removeNode(this.rootNode, key)
        }
    }

}

let bst = new BinarySearchTree()
//let nums = [5, 4, 6, 2, 8, 1, 3, 7, 9]
//let nums = [11, 7, 15, 5, 9, 13, 20, 3, 6, 8, 10, 12, 14, 18, 25]
let nums = [11, 15, 5, 13, 20, 3, 6, 12, 14, 18, 25]    // 刪除的節點沒有右側子樹

for (num of nums) {
    bst.insert(num)
}

//bst.inOrderIter()
//bst.rootFirstIter()
//bst.leafFirstIter()

/*console.log(bst.min())  // 1
console.log(bst.max())  // 9

console.log(bst.has(3))     // true
console.log(bst.has(11))    // false
console.log(bst.getNode(3).key) // 3
*/

// 刪除帶有子節點的節點
//bst.remove(5)

// 刪除帶有子樹的節點
//bst.remove(15)
bst.remove(7)

bst.inOrderIter()

module.exports = BinarySearchTree
