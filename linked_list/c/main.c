
#define ERROR 1
#define OK 0

#include <stdio.h>

// typedef old-type-name new-type-name

typedef int ElemType;

typedef struct Node
{
    ElemType data;
    struct Node* next;
} Node;

typedef struct Node* LinkList;
/*
typedef struct LinkList
{
    Node *head;
} LinkList;
*/

int GetElem( LinkList L, int i, ElemType *e)
{
    int j;
    LinkList p;

    p = L->next;
    j = 1;

    while(p && j<i)
    {
        p = p->next;
        ++j;
    }

    if (!p || j>i)
    {
        return ERROR;
    }

    *e = p->data;

    return OK;
}

int main()
{
    LinkList ll;
    ElemType *result;

    GetElem(ll, 0, result);

    printf("%d", *result);
}