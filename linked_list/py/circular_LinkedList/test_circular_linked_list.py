from pytest import fixture, raises, mark
from circular_linked_list import Node, Circular_Linked_List

@fixture
def empty_link_list():
    return Circular_Linked_List()

@fixture
def link_list(empty_link_list):
    cll = empty_link_list
    cll.tailInsert(1)
    cll.tailInsert(3)
    cll.tailInsert(5)
    return cll

class Test_Circular_Linked_List:
    
    def test_headInsert(self, empty_link_list):
        cll = empty_link_list
        cll.headInsert(1)       # 頭部插入 [1]
        cll.headInsert(2)       # 頭部插入 [2,1]

        assert cll.iterator() == [2,1]

    def test_tailInsert(self, empty_link_list):
        cll = empty_link_list
        cll.tailInsert(1)       # 頭部插入 [1]
        cll.tailInsert(2)       # 頭部插入 [1,2]

        assert cll.iterator() == [1,2]

    def test_insert(self, link_list):
        cll = link_list # cll = [1,3,5]

        # 測試頭插法
        cll.insert(0, "a")
        assert cll.iterator() == ["a",1,3,5]

        # 測試 negative-index
        cll.insert(-1, "b")
        assert cll.iterator() == ["a",1,3,5,"b"]

        # 測試尾插法
        cll.insert(4, "c")
        assert cll.iterator() == ["a",1,3,5,"b","c"]

        # 測試中間插法
        cll.insert(2, "d")
        assert cll.iterator() == ["a",1,"d",3,5,"b","c"]

    def test_insert_error(self, link_list):
        cll = link_list
        with raises(AssertionError, match="index is out of range"):
            # cll = [1,3,5]
            cll.insert(3, "a")

        with raises(AssertionError, match="index is out of range"):
            # cll = [1,3,5]
            cll.insert(-4, "a")

    def test_isEmpty(self, empty_link_list):
        cll = empty_link_list
        assert cll.isEmpty() == True

        cll.headInsert(5)
        assert cll.isEmpty() == False

    def test_length(self, empty_link_list):
        cll = empty_link_list
        assert cll.length() == 0

        cll.headInsert(1)
        assert cll.length() == 1

        cll.headInsert(2)
        assert cll.length() == 2

    def test_deleteByIndex(self, link_list):
        cll = link_list # cll = [1,3,5]
        cll.deleteByIndex(0)
        assert cll.iterator() == [3,5]

        cll.deleteByIndex(1)
        assert cll.iterator() == [3]

        cll.deleteByIndex(0) 
        assert cll.iterator() == []

    def test_deleteByIndex_error(self, empty_link_list):
        cll = empty_link_list 

        with raises(AssertionError, match="circular-linked-list is empty"):
            cll.deleteByIndex(0)

        cll.tailInsert("a")

        with raises(AssertionError, match="index is out of range"):
            cll.deleteByIndex(1)

    @mark.parametrize("index, expect", [(-1,5),(0,1),(1,3),(2,5)])
    def test_getElement(self, index, expect, link_list):
        assert link_list.getElement(index) == expect

    def test_getElement_error(self, empty_link_list):
        cll = empty_link_list

        with raises(AssertionError, match="index is out of range"):
            cll.getElement(1)

    def test_setElement(self, link_list):
        link_list.setElement(0,"a")
        assert link_list.iterator() == ["a", 3, 5]

        link_list.setElement(2,"c")
        assert link_list.iterator() == ["a", 3, "c"]

    def test_setElement_error(self, link_list):
        with raises(AssertionError, match="index is out of range"):
            link_list.setElement(3,"a")

    @mark.parametrize("value, expect", [(1,0), (2,None)])
    def test_search(self, value, expect, link_list):
        cll = link_list
        assert cll.search(value) == expect

    def test_new(self, empty_link_list):
        cll = empty_link_list
        cll.new([2,4,6])
        assert cll.iterator() == [2,4,6]

    def test_clear(self, link_list):
        cll = link_list
        assert cll.clear() == []

    def test_joseph_circle(self, empty_link_list):
        cll = empty_link_list
        assert cll.joseph_circle([1,2,3,4,5,6,7,8,9,10], 2) == [2,4,6,8,10,3,7,1,9,5]

        cll.clear()
        assert cll.joseph_circle([1,2,3,4,5,6,7,8,9,10], 3) == [3,6,9,2,7,1,8,5,10,4]
