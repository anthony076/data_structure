from typing import Any, List
from random import randint

# 定義單鍊表中的節點
class Node(object):
    def __init__(self, data=None):

        # 指向下一個數據的指針
        self.next = None

        # 儲放數據
        self.data = data

# 定義單鍊表，
class Circular_Linked_List(object):
    def __init__(self):
        # 頭節點，指向第一個節點，為第一個節點的引用
        self.head = Node()
        self.head.next = self.head

    def _negative_index(self, index):
        length = self.length()

        if index < 0 :
            if abs(index) > length:
                assert False, "index is out of range.."
            else:
                index = index + length
                return index
        else:
            return index

    def isEmpty(self) -> bool:
        if self.head.next == self.head :
            return True
        else:
            return False

    def length(self) -> int:
        count = 0
        cur = self.head

        # 檢查是否道尾節點，若未到尾節點，移動工作指針 cur
        while cur.next != self.head:
            cur = cur.next
            count += 1

        return count
            
    # 返回當前鍊表所有的數據內容，以 list 的方式呈現
    def iterator(self):
        result = []
        
        # 注意，鍊表的第一個節點沒有存放元素，所以工作指針應該指向 head.next
        # head.next 才是第一個節點
        curNode = self.head.next
        
        for i in range(self.length()):
            result.append(curNode.data)
            curNode = curNode.next

        return result

    # 往頭部插入節點
    def headInsert(self, data):
        # 建立新節點
        node = Node(data)

        # 將新節點加到鍊表中
        node.next = self.head.next
        self.head.next = node

    # 往尾部插入節點
    def tailInsert(self, data):
        node = Node(data)

        # 注意，鍊表的第一個節點沒有存放元素，所以工作指針應該指向 head.next
        # head.next 才是第一個節點
        curNode = self.head.next

        while curNode.next != self.head:
            curNode = curNode.next
        
        curNode.next = node
        node.next = self.head

    # 指定位置插入節點
    def insert(self, index:int, data:Any):
        length = self.length()

        # negative index 轉換
        index = self._negative_index(index)
        print(index)
        
        # index = 0 ，頭插法 ，index = end ，尾插法
        if index == 0:
            self.headInsert(data)
        elif index == length -1:
            self.tailInsert(data)
        elif index >= length:
            assert False, "index is out of range.."
        else:
            # 建立新節點
            node = Node(data)

            # 注意，鍊表的第一個節點沒有存放元素，所以工作指針應該指向 head.next
            # head.next 才是第一個節點
            curNode = self.head.next

            # 插入時，會將工作指真 curNode 停在 index -1 的節點
            # range(1, index)，因為 index = 0 會直接進入 headInsert()，
            # 不會到此區域，進到此區域一定是 index = 1 開始
            for i in range(1, index):
                curNode = curNode.next

            node.next = curNode.next
            curNode.next = node

    # 刪除指定位置節點
    def deleteByIndex(self, index:int):
        length = self.length()

        # 檢查是否為空鍊表
        if self.isEmpty():
            assert False, "circular-linked-list is empty"

        else:
            # negative index 轉換
            if index < 0 :
                if abs(index) > length:
                    assert False, "index is out of range.."
                else:
                    index = index + length
            
            # 設定工作指針指向第一節點
            curNode = self.head.next

            if index == 0:
                self.head.next = curNode.next

            elif index >= length:
                assert False, "index is out of range.."
            
            else:
                # For index < length -1 

                # 注意，刪除位置需要將工作指針 curNode 停在指定位置的前一位，
                # 因此，index-1
                for i in range(1, length-1):
                    curNode = curNode.next

                curNode.next = self.head

    def getElement(self, index:int):
        length = self.length()

        # ==== negative-index-transform ====
        index = self._negative_index(index)

        # ==== check index ====
        if index >= length:
            assert False, "index is out of range..."
        
        # ==== getElement ====
        curNode = self.head.next

        for i in range(1, index+1):
            curNode = curNode.next

        return curNode.data

    def setElement(self, index:int, data:Any):
        length = self.length()

        # ==== negative-index-transform ====
        index = self._negative_index(index)

        # ==== check index ====
        if index >= length:
            assert False, "index is out of range..."
        
        # ==== setElement ====
        curNode = self.head.next

        for i in range(1, index+1):
            curNode = curNode.next

        curNode.data = data

    def search(self, data):
        if self.isEmpty():
            return None
        else:
            count = 0
            # 設定工作節點
            curNode = self.head.next

            # 移動工作節點至尾節點
            while curNode.next != self.head:

                if curNode.data == data:
                    return count
                    
                curNode = curNode.next
                
                count +=1
                
            return None

    def new(self, datas:List):
        if not self.isEmpty():
            self.clear()
            
        for data in datas:
            self.tailInsert(data)
        return self

    def clear(self):
        curNode = self.head.next

        while not self.isEmpty():
            nextNode = curNode.next
            self.head.next = nextNode
            del curNode

            curNode = nextNode

        return self.iterator()

    def joseph_circle(self, dataList, step):

        counter = 1
        result = []

        # 產生循環鍊表
        self.new(dataList)
        print(f"{self.iterator()}, step={step}")

        # 將工作指針指向第一個節點
        curNode = self.head.next

        while len(result) != self.length():

            # ==== 判斷是否已經列舉過 =====
            # 已經列舉過
            if curNode.data in result:
                pass

            # 未列舉過
            else:
                # 未列舉過的節點，需要等 counter 計數到 step 值才取當前節點的內容
                if (counter % step > 0):
                    counter += 1
                
                # 未列舉過的節點，且 counter == step
                else:
                    result.append(curNode.data)
                    counter = 1
            
            # 已經到達尾節點，重新將位置定位到第一節點而非頭節點
            # 注意，若沒有加入此行，尾節點的下一個位置是頭節點，內容為 None
            if curNode.next == self.head:
                curNode = self.head.next
            else:
                # 移動到下一個節點
                curNode = curNode.next

        return result

if __name__ == "__main__":
    cll = Circular_Linked_List()

    # [1,2,3,4,5,6,7,8,9,10], step = 2 得到 [2,4,6,8,10,3,7,1,9,5]
    print("joseph: ", cll.joseph_circle([1,2,3,4,5,6,7,8,9,10], 2))
    print()
    
    # [1,2,3,4,5,6,7,8,9,10], step = 3 得到 [3,6,9,2,7,1,8,5,10,4]
    print("joseph: ", cll.joseph_circle([1,2,3,4,5,6,7,8,9,10], 3))
    print()
    
    print("joseph: ", cll.joseph_circle([item for item in range(1,42)], 3))



