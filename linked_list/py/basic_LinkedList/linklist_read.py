
from typing import Any, List

class Node(object):
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkList(object):
    def __init__(self):
        self.head = None
    
    def append(self, data:Any):
        # 建立新節點
        node = Node(data)

        if self.head == None:
            self.head = node
        else:
            # 定位工作指針
            cur = self.head
            
            # 移動工作指針到尾節點
            while cur.next != None:
                cur = cur.next

            # 插入新節點
            cur.next = node

    def length(self):
        count = 0
        
        # 定位工作指針
        cur = self.head

        # 移動工作指針到尾節點
        while cur != None:
            count += 1
            cur = cur.next

        return count

    def iter(self):
        result = []
        
        # 定位工作指針
        cur = self.head

        # 移動工作指針到尾節點
        while cur != None:
            result.append(cur.data)
            cur = cur.next
        
        print(result)

def getItem_m1(LinkList, index:int) -> Any:
    # ==== support negative-index ====
    if index < 0:
        if abs(index) > LinkList.length():
            assert False, "index in out of range"
        else:
            index = index + 3
    
    # ==== flow ====
    cur = LinkList.head

    for i in range(index):
        cur = cur.next
    
    return cur.data

def getItem_m2(LinkList, index:int) -> Any:
    # ==== support negative-index ====
    if index < 0:
        if abs(index) > LinkList.length():
            assert False, "index in out of range"
        else:
            index = index + 3

    # ==== flow ====
    # 當前節點
    curNode = LinkList.head

    # 計數移動次數
    j = 0

    while (curNode.next != None) & (j < index):
        #移動當前節點
        curNode = curNode.next
        j = j+1

    return curNode.data

if __name__ == "__main__":
    ll = LinkList()

    ll.append(1)
    ll.append(2)
    ll.append(3)
    ll.iter()

    assert getItem_m1(ll, 0) == 1
    assert getItem_m1(ll, 1) == 2
    assert getItem_m1(ll, 2) == 3
    assert getItem_m1(ll, -1) == 3

    assert getItem_m1(ll, 0) == 1
    assert getItem_m1(ll, 1) == 2
    assert getItem_m1(ll, 2) == 3





