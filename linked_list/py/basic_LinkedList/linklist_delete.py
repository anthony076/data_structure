
from typing import Any, List

class Node(object):
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkList(object):
    def __init__(self):
        self.head = None
    
    def append(self, data:Any):
        # 建立新節點
        node = Node(data)

        if self.head == None:
            self.head = node
        else:
            # 定位工作指針
            cur = self.head
            
            # 移動工作指針到尾節點
            while cur.next != None:
                cur = cur.next

            # 插入新節點
            cur.next = node

    def length(self):
        count = 0
        
        # 定位工作指針
        cur = self.head

        # 移動工作指針到尾節點
        while cur != None:
            count += 1
            cur = cur.next

        return count

    def iter(self):
        result = []
        
        # 定位工作指針
        cur = self.head

        # 移動工作指針到尾節點
        while cur != None:
            result.append(cur.data)
            cur = cur.next
        
        print(result)
        return result

# ===== 參數要求，LinkList 不能為空=====
def linklist_limit(LinkList):
    if LinkList.length() == 0:
        assert False, "linklist is empty"

# ====  參數要求，index 可為負，但不能大於鍊表長 ====
def index_limit(LinkList, index):
    if index < 0:
        if abs(index) > LinkList.length():
            assert False, "index in out of range"
        else:
            index = index + LinkList.length()
    
    return index

def deleteItem_m1(LinkList, index:int) -> Any:
    linklist_limit(LinkList)
    index = index_limit(LinkList, index)

    # ==== flow ====
    # 定義工作指針
    preNode = LinkList.head

    if index == 0:
        LinkList.head = preNode.next
    else:
        #移動工作指針(preNode)到指定位置(index)的前一個位置
        for i in range(index-1):
            preNode = preNode.next

    # 取得目標位置
    targetNode = preNode.next
    # 將 preNode.next 指向 目標位置的 next 指針
    preNode.next = targetNode.next

def deleteItem_m2(LinkList, index:int) -> Any:
    linklist_limit(LinkList)
    index = index_limit(LinkList, index)

    # preNode 指定位置的前一個節點
    preNode = LinkList.head

    # 工作指針，因為當 index = 0，沒有前一個節點，index = 0 需要另外處理
    # 因此 工作指針從 1 開始
    j = 1

    if index == 0:
        # 直接將 head 指向第二個節點
        LinkList.head = LinkList.head.next
    else:
        while (preNode.next != None) & (j < index):
            # 將當前節點移動到下一個節點
            preNode = preNode.next
            j = j + 1
        
        targetNode = preNode.next
        preNode.next = targetNode.next

    LinkList.iter()

if __name__ == "__main__":
    pass




