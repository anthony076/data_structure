from pytest import fixture, raises, mark
from linklist_insert import LinkList, headInsert, tailInsert, indexInsert_m1, indexInsert_m2

@fixture
def empty_linklist():
    return LinkList()

@fixture
def linklist(empty_linklist):
    ll = empty_linklist

    ll.append(1)
    ll.append(3)
    ll.append(5)

    return ll

def test_headInsert(linklist):
    ll = linklist
    assert headInsert(ll, 5) == [5,1,3,5]

def test_tailInsert(linklist):
    ll = linklist
    assert tailInsert(ll, 6) == [1,3,5,6]

def test_indexInsert_m1(empty_linklist):
    ll = empty_linklist

    assert indexInsert_m1(ll, 0, 3) == [3]
    assert indexInsert_m1(ll, 0, 1) == [1,3]
    assert indexInsert_m1(ll, 1, 2) == [1,3,2]
    assert indexInsert_m1(ll, -1, 2) == [1,3,2,2]

def test_indexInsert_m2(empty_linklist):
    ll = empty_linklist

    assert indexInsert_m2(ll, 0, 3) == [3]
    assert indexInsert_m2(ll, 0, 1) == [1,3]
    assert indexInsert_m2(ll, 1, 2) == [1,3,2]
    assert indexInsert_m2(ll, -1, 2) == [1,3,2,2]

