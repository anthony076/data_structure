from pytest import fixture, raises, mark
from linklist_read import LinkList, getItem_m1, getItem_m2

@fixture
def empty_linklist():
    return LinkList()

@fixture
def linklist(empty_linklist):
    ll = empty_linklist

    ll.append(1)
    ll.append(3)
    ll.append(5)

    return ll

@mark.parametrize("index, value", [(0,1), (1,3), (2,5), (-1,5), (-2, 3), (-3, 1)])
def test_getItem(index, value, linklist):
    ll = linklist

    assert getItem_m1(ll, index) == value
    assert getItem_m2(ll, index) == value

