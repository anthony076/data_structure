from pytest import fixture, raises, mark
from linklist_write import LinkList, setItem_m1, setItem_m2

@fixture
def empty_linklist():
    return LinkList()

@fixture
def linklist(empty_linklist):
    ll = empty_linklist

    ll.append(1)
    ll.append(3)
    ll.append(5)

    return ll

@mark.parametrize("index, newValue, returnValue", [(0,1,1), (1,2,2), (2,3,3), (-1,3,3), (-2,2,2), (-3,1,1)])
def test_setItem(index, newValue, returnValue, linklist):
    ll = linklist

    assert setItem_m1(ll, index, newValue) == returnValue
    assert setItem_m2(ll, index, newValue) == returnValue

