
from typing import Any, List

class Node(object):
    def __init__(self, data:Any):
        self.data = data
        self.next = None

class LinkList(object):
    def __init__(self):
        self.head = None
    
    def append(self, data:Any):
        # 建立新節點
        node = Node(data)

        if self.head == None:
            self.head = node
        else:
            # 定位工作指針
            cur = self.head
            
            # 移動工作指針到尾節點
            while cur.next != None:
                cur = cur.next

            # 插入新節點
            cur.next = node

    def length(self):
        count = 0
        
        # 定位工作指針
        cur = self.head

        # 移動工作指針到尾節點
        while cur != None:
            count += 1
            cur = cur.next

        return count

    def iter(self):
        result = []
        
        # 定位工作指針
        cur = self.head

        # 移動工作指針到尾節點
        while cur != None:
            result.append(cur.data)
            cur = cur.next
        
        print(result)
        return result

# ====  參數要求，index 可為負，但不能大於鍊表長 ====
def index_limit(LinkList:LinkList, index:int):
    if abs(index) > LinkList.length():
        assert False, "index in out of range"
    elif index < 0:
        index = index + LinkList.length()
    else:
        pass

    return index

def headInsert(LinkList, data) -> Any:
    node = Node(data)
    cur = LinkList.head

    node.next = LinkList.head
    LinkList.head = node

    return LinkList.iter()

def tailInsert(LinkList:LinkList, data:Any) -> Any:
    node = Node(data)
    cur = LinkList.head

    while cur.next != None:
        cur = cur.next

    cur.next = node

    return LinkList.iter()

def indexInsert_m1(LinkList:LinkList, index:int, data:Any) -> Any:
    index = index_limit(LinkList, index)

    node = Node(data)   # 建立節點
    cur = LinkList.head # 定位到頭節點

    # 空鍊表，不用移動
    if cur == None:
        # 直接將頭節點指向新節點
        LinkList.head = node

    # 插入第一個節點 (頭插入)，不需要移動
    elif index == 0 :
        node.next = LinkList.head
        LinkList.head = node

    # 尾插入
    elif index == LinkList.length()-1:
        while cur.next != None:
            cur = cur.next

        cur.next = node

    # 中間插入
    else:
        for i in range(index):
            cur = cur.next
        node.next = cur.next
        cur.next = node

    return LinkList.iter()

def indexInsert_m2(LinkList:LinkList, index:int, data:Any) -> Any:
    index = index_limit(LinkList, index)

    node = Node(data)   
    curNode = LinkList.head 

    if curNode == None:
        LinkList.head = node

    elif index == 0 :
        node.next = LinkList.head
        LinkList.head = node

    elif index == LinkList.length()-1:
        while curNode.next != None:
            curNode = curNode.next

        curNode.next = node

    # 中間插入
    else:

        # 移動到目標位置
        j = 1
        while (curNode.next != None) & ( j < index):
            curNode = curNode.next
            j += 1

        node.next = curNode.next
        curNode.next = node

    return LinkList.iter()

if __name__ == "__main__":
    pass