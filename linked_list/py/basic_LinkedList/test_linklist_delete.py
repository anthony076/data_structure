from pytest import fixture, raises, mark
from linklist_delete import LinkList, deleteItem_m1, deleteItem_m2

@fixture
def empty_linklist():
    return LinkList()

@fixture
def linklist(empty_linklist):
    ll = empty_linklist

    ll.append(1)
    ll.append(3)
    ll.append(5)

    return ll

@mark.parametrize("index, expect", [(0,[3,5]),(1,[1,5]),(2,[1,3]),(-1,[1,3])])
def test_deleteItem_m1(index, expect, linklist):
    ll = linklist

    deleteItem_m1(ll, index)
    assert  ll.iter() == expect

@mark.parametrize("index, expect", [(0,[3,5]),(1,[1,5]),(2,[1,3]),(-1,[1,3])])
def test_deleteItem_m2(index, expect, linklist):
    ll = linklist

    deleteItem_m2(ll, index)
    assert  ll.iter() == expect
