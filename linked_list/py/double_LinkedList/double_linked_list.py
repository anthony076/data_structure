from typing import Any, List
from random import randint

# 定義單鍊表中的節點
class Node(object):
    def __init__(self, data=None):
        # 指向上一個數據的指針
        self.pre = None

        # 指向下一個數據的指針
        self.next = None

        # 儲放數據
        self.data = data

# 定義單鍊表，
class Double_Linked_List(object):
    def __init__(self):
        # 頭節點，指向第一個節點，為第一個節點的引用
        self.head = Node()
        self.head.next = self.head
        self.head.pre = self.head

    def _negative_index(self, index):
        pass

    def isEmpty(self) -> bool:
        if (self.head.next == self.head) & (self.head.pre == self.head):
            return True
        else:
            return False

    def length(self) -> int:
        count = 0
        curNode = self.head.next

        while curNode.next != self.head:
            count += 1  
        
        return count

    # 返回當前鍊表所有的數據內容，以 list 的方式呈現
    def iterator(self):
        pass

    # 往頭部插入節點
    def headInsert(self, data):
        

    # 往尾部插入節點
    def tailInsert(self, data):
        pass

    # 指定位置插入節點
    def insert(self, index:int, data:Any):
        pass

    # 刪除指定位置節點
    def deleteByIndex(self, index:int):
        pass

    def getElement(self, index:int):
        pass

    def setElement(self, index:int, data:Any):
        pass

    def search(self, data):
        pass

    def new(self, datas:List):
        pass

    def clear(self):
        pass


if __name__ == "__main__":
    dll = Double_Linked_List()


