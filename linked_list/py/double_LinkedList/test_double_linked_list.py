from pytest import fixture, raises, mark
from double_linked_list import Node, Double_Linked_List

@fixture
def empty_link_list():
    return Double_Linked_List()

@fixture
def link_list(empty_link_list):
    dll = empty_link_list
    dll.tailInsert(1)
    dll.tailInsert(3)
    dll.tailInsert(5)
    return dll

class Test_Circular_Linked_List:
    
    def test_headInsert(self, empty_link_list):
        dll = empty_link_list
        dll.headInsert(1)       # 頭部插入 [1]
        dll.headInsert(2)       # 頭部插入 [2,1]

        assert dll.iterator() == [2,1]

    def test_tailInsert(self, empty_link_list):
        dll = empty_link_list
        dll.tailInsert(1)       # 頭部插入 [1]
        dll.tailInsert(2)       # 頭部插入 [1,2]

        assert dll.iterator() == [1,2]

    def test_insert(self, link_list):
        dll = link_list # dll = [1,3,5]

        # 測試頭插法
        dll.insert(0, "a")
        assert dll.iterator() == ["a",1,3,5]

        # 測試 negative-index
        dll.insert(-1, "b")
        assert dll.iterator() == ["a",1,3,5,"b"]

        # 測試尾插法
        dll.insert(4, "c")
        assert dll.iterator() == ["a",1,3,5,"b","c"]

        # 測試中間插法
        dll.insert(2, "d")
        assert dll.iterator() == ["a",1,"d",3,5,"b","c"]

    def test_insert_error(self, link_list):
        dll = link_list
        with raises(AssertionError, match="index is out of range"):
            # dll = [1,3,5]
            dll.insert(3, "a")

        with raises(AssertionError, match="index is out of range"):
            # dll = [1,3,5]
            dll.insert(-4, "a")

    def test_isEmpty(self, empty_link_list):
        dll = empty_link_list
        assert dll.isEmpty() == True

        #dll.headInsert(5)
        #assert dll.isEmpty() == False

    def test_length(self, empty_link_list):
        dll = empty_link_list
        assert dll.length() == 0

        dll.headInsert(1)
        assert dll.length() == 1

        dll.headInsert(2)
        assert dll.length() == 2

    def test_deleteByIndex(self, link_list):
        dll = link_list # dll = [1,3,5]
        dll.deleteByIndex(0)
        assert dll.iterator() == [3,5]

        dll.deleteByIndex(1)
        assert dll.iterator() == [3]

        dll.deleteByIndex(0) 
        assert dll.iterator() == []

    def test_deleteByIndex_error(self, empty_link_list):
        dll = empty_link_list 

        with raises(AssertionError, match="circular-linked-list is empty"):
            dll.deleteByIndex(0)

        dll.tailInsert("a")

        with raises(AssertionError, match="index is out of range"):
            dll.deleteByIndex(1)

    @mark.parametrize("index, expect", [(-1,5),(0,1),(1,3),(2,5)])
    def test_getElement(self, index, expect, link_list):
        assert link_list.getElement(index) == expect

    def test_getElement_error(self, empty_link_list):
        dll = empty_link_list

        with raises(AssertionError, match="index is out of range"):
            dll.getElement(1)

    def test_setElement(self, link_list):
        link_list.setElement(0,"a")
        assert link_list.iterator() == ["a", 3, 5]

        link_list.setElement(2,"c")
        assert link_list.iterator() == ["a", 3, "c"]

    def test_setElement_error(self, link_list):
        with raises(AssertionError, match="index is out of range"):
            link_list.setElement(3,"a")

    @mark.parametrize("value, expect", [(1,0), (2,None)])
    def test_search(self, value, expect, link_list):
        dll = link_list
        assert dll.search(value) == expect

    def test_new(self, empty_link_list):
        dll = empty_link_list
        dll.new([2,4,6])
        assert dll.iterator() == [2,4,6]

    def test_clear(self, link_list):
        dll = link_list
        assert dll.clear() == []

    def test_joseph_circle(self, empty_link_list):
        dll = empty_link_list
        assert dll.joseph_circle([1,2,3,4,5,6,7,8,9,10], 2) == [2,4,6,8,10,3,7,1,9,5]

        dll.clear()
        assert dll.joseph_circle([1,2,3,4,5,6,7,8,9,10], 3) == [3,6,9,2,7,1,8,5,10,4]
