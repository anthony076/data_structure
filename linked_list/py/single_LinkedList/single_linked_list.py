from typing import Any, List

# 定義單鍊表中的節點
class Node(object):
    def __init__(self, data=None):

        # 指向下一個數據的指針
        self.next = None

        # 儲放數據
        self.data = data

# 定義單鍊表，
class Single_Linked_List(object):
    def __init__(self):
        # 頭節點，指向第一個節點，為第一個節點的引用
        self.head = None

    def new(self, datas:List):
        for data in datas:
            self.append(data)
        print(self.travel())

    def getElement(self, index:int):
        # 空鍊表，觸發 Error
        if self.head == None:
            assert False, "empty linked-list"

        # index <= 0，代表在第一個節點
        if index <= 0 :
            return self.head.data

        else:
            # 將工作指針指向頭節點
            cur = self.head

            # 移動工作指針
            for i in range(index):
                cur = cur.next

            # 到達位置後，返回該節點中的資料
            return cur.data

    def setElement(self, index:int, data:Any):
        # index must >= 0
        if index < 0 :
            assert False, "index must >=0"

        # 將工作指針指向頭節點
        cur = self.head

        # 移動工作指針
        for i in range(index):
            cur = cur.next

        # 將資料寫入節點
        cur.data = data

    def length(self) -> int:
        # 鍊表不會預先分配內存空間，因此沒有固定長度
        # 必須透過遍歷來計算當前鍊表的長度
        count = 0
        cur = self.head

        while cur != None:
            count += 1
            cur = cur.next

        return count

    # 返回當前鍊表所有的數據內容，以 list 的方式呈現
    def travel(self):
        cur = self.head

        result = []
        while cur != None:
            result.append(cur.data)
            cur = cur.next

        return result

    # 往頭部插入節點
    def add(self, data):
        #1  插入前 head -> [null]
        #   插入後 head -> [data, next] -> [null]
        #2  add 是將節點往前插入，被插入的節點會擺在第一個，
        #   頭節點(self.head)永遠指向第一個節點

        # 建立新節點
        node = Node(data)

        # 新節點的 next 指向前一個 頭節點(self.head) 指向的節點
        node.next = self.head

        # 將頭節點(self.head)指向新節點
        self.head = node

    # 往尾部插入節點
    def append(self, data):
        #1  插入前 head -> [data1, next1] -> [null]
        #   插入後 head -> [data1, next1] -> [data2, next2] -> [null]

        node = Node(data)

        # 若是空鍊表，直接將 self.head 指向新節點
        if self.isEmpty():
            self.head = node
        else:
            # 將指針定位到頭指針
            cur = self.head

            # 移動到尾節點
            while cur.next != None:
                cur = cur.next

            # 已到達尾節點，將新節點的 next 指向新節點
            cur.next = node

    # 指定位置插入節點
    def insert(self, index:int, data):
        # 插入前 head -> [data1, next1] -> [data2, next2] -> [null]
        # 插入後 head -> [data1, next1] -> [data3, next3] -> [data2, next2] -> [null]
        node = Node(data)

        # ==== 指定位置在第一個位置，執行頭部插入 ====
        if index <= 0:
            self.add(data)
        # ==== 指定位置在最後位置，執行尾部插入 ====
        elif index > self.length() - 1:
            self.append(data)
        # ==== 指定位置中間位置 ====
        else:
            cur = self.head

            # 移動到指定位置
            for i in range(index-1):
                cur = cur.next

            # ==== 重新配置指針的指向 ====
            # 新節點的 next 指向當前位置的 next
            node.next = cur.next
            # 當前位置的 next 指向 新節點
            cur.next = node

    # 刪除指定位置節點
    def deleteByIndex(self, index:int):
        # 刪除前 head -> [data1, next1] -> [data3, next3] -> [data2, next2] -> [null]
        # 刪除後 head -> [data1, next1] -> [data2, next2] -> [null]

        pre = self.head

        # 移動到指定位置的前一個位置
        for i in range(index-1):
            pre = pre.next

        target = pre.next
        pre.next = target.next

    def isEmpty(self) -> bool:
        return self.head == None

    def search(self, data):
        #移動到頭節點
        cur = self.head

        # 判斷是否是空鍊表
        while cur != None:
            if cur.data == data:
                return True
            cur = cur.next

        return False

    def clear(self):

        # curNode 永遠指向第一個節點
        curNode = self.head

        while curNode != None:
            # 指向 curNode 的下一個節點
            nextNode = curNode.next
            # 刪除第一個節點 
            del curNode
            curNode = nextNode
        
        self.head = curNode

        if self.isEmpty():
            return "all clear"
        else:
            self.travel()
            return "not empty"
        
if __name__ == "__main__":
    pass
