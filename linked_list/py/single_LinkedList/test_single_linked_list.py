from pytest import fixture, raises, mark
from single_linked_list import Node, Single_Linked_List

@fixture
def empty_link_list():
    return Single_Linked_List()

@fixture
def link_list(empty_link_list):
    ll = empty_link_list
    ll.append(1)
    ll.append(3)
    ll.append(5)
    return ll

class Test_Single_Linked_List:
    def test_add_node(self, empty_link_list):
        ll = empty_link_list
        ll.add(1)       # 頭部插入 [1]
        ll.add(2)       # 頭部插入 [2,1]
        ll.append(3)    # 尾部插入 [2,1,3]
        ll.insert(1,4)  # 指定位置插入 [2,4,1,3]
        assert ll.travel() == [2,4,1,3]

    def test_isEmpty(self, empty_link_list):
        ll = empty_link_list
        assert ll.isEmpty() == True

        ll.add(5)
        assert ll.isEmpty() == False

    def test_length(self, empty_link_list):
        ll = empty_link_list
        assert ll.length() == 0

        ll.add(1)
        assert ll.length() == 1

    def test_deleteByIndex(self, link_list):
        ll = link_list
        ll.deleteByIndex(1)
        assert ll.travel() == [1,5]

    @mark.parametrize("value, expect", [(1,True), (2,False)])
    def test_search(self, value, expect, link_list):
        ll = link_list

        assert ll.search(value) == expect

    @mark.parametrize("index, expect", [(-1,1),(0,1),(1,3),(2,5),])
    def test_getElement_usage(self, index, expect, link_list):
        assert link_list.getElement(index) == expect

    def test_getElement_usage_error(self, empty_link_list):
        ll = empty_link_list

        with raises(AssertionError, match="empty linked-list"):
            ll.getElement(0)

    def test_setElement_usage(self, link_list):
        link_list.setElement(0,"a")
        assert link_list.travel() == ["a", 3, 5]

        link_list.setElement(2,"c")
        assert link_list.travel() == ["a", 3, "c"]

    def test_setElement_error(self, link_list):
        with raises(AssertionError, match="index must >=0"):
            link_list.setElement(-1,"a")

    def test_new(self, empty_link_list):
        ll = empty_link_list
        ll.new([2,4,6])
        assert ll.travel() == [2,4,6]

    def test_clear(self, link_list):
        ll = link_list
        assert ll.clear() == "all clear"
