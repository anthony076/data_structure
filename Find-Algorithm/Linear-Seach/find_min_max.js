const Dataset = require("../../Sort-Algorithm/dataSet")

function getMinMax(arr, mode) {
    var target = arr[0]

    for (var i = 0; i < arr.length; i++) {
        if (mode === "min") {
            if (arr[i] < target) {
                target = arr[i]
            }
        } else if (mode == "max") {
            if (arr[i] > target) {
                target = arr[i]
            }
        } else {
            throw new Error("mode is not correct.")
        }
    }
    return target
}

const d = new Dataset()
d.toString()
console.log(getMinMax(d.arr, "min"))
console.log(getMinMax(d.arr, "max"))