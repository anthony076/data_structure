
arr = [1, 2, 3, 4, 5]

function linearSearch(arr, num) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === num) {
            return i
        }
    }

    return -1
}


function swap(arr, i, j) {
    sum = arr[i] + arr[j]
    arr[i] = sum - arr[i]
    arr[j] = sum - arr[j]
}

// 自組織數據，每次找到目標，就將該目標的位置往前移動一位
// 結果是越常被查詢的資料，就會排的越前面，因此下次查詢的速度就會愈快
function seqSearch(arr, num) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === num) {
            // 讓目標元素往前移動
            swap(arr, i, i - 1)
            return i
        }
    }

    return -1
}

// 設定比例的自組織數據，每次找到目標，判斷當前位置是否位於總長度的 20%，
// 若是，才將該目標的位置往前移動一位，用於減少交換的次數
function better_seqSearch(arr, num) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === num && i > arr.length * 0.2) {
            // 若當前位置i，位於總長度的20%以外，才讓目標元素往前移動
            swap(arr, i, i - 1)
            return i
        } else if (arr[i] === num) {
            // 若當前位置i，不在總長度的20%內，不將元素移動，直接返回 index
            return i
        }
    }

    return -1
}

console.log(linearSearch(arr, 6))

seqSearch(arr, 5)
console.log(arr)    // [ 1, 2, 3, `5`, 4 ]

seqSearch(arr, 5)
console.log(arr)    // [ 1, 2, `5`, 3, 4 ]

seqSearch(arr, 5)
console.log(arr)    // [ 1, `5`, 2, 3, 4 ]

// 數字5，位於前20%的位置，所以不移動
better_seqSearch(arr, 5)
console.log(arr)    // [ 1, `5`, 2, 3, 4 ]

// 數字4，不在前20%的位置內，因此往前移動
better_seqSearch(arr, 4)
console.log(arr)    // [ 1, 5, 2, `4`, 3 ]
