const Dataset = require("../../Sort-Algorithm/dataSet")

// 自我嘗試版
function myBinarySearch(arr, target) {

    let iMax = arr.length - 1,
        iMin = 0,
        iMid = Math.floor((iMax - iMin) / 2);

    // ==== 如果搜尋目標在頭尾兩端，會找不到 ====
    if (arr[0] === target) {
        return `found, number:${target} @ index:0`
    } else if (arr[arr.length - 1] === target) {
        return `found, number:${target} @ index:${arr.length - 1}`
    }

    // 二分法搜尋，比較目標與中值
    // 若目標比較大=目標在右側，就縮小左側邊界，並設定新的中值
    // 若目標比較小=目標在左側，就縮小右側邊界，並設定新的中值
    // 當 iMid != iMin，代表已經移動結束
    let moveCount = 0;
    while (iMid != iMin) {
        if (arr[iMid] < target) {
            iMin = iMid
        } else if (arr[iMid] > target) {
            iMax = iMid
        } else {
            break
        }

        iMid = iMin + Math.floor((iMax - iMin) / 2)
        moveCount += 1
    }
    console.log(`moveCount:${moveCount}`)

    // 判斷是找到目標，或是未找到目標
    if (arr[iMid] === target) {
        return `found, number:${target} @ index:${iMid}`
    } else {
        return `number:${target}, Not found`
    }

}

// 簡化版
function simpleBinarySearch(arr, target) {
    var upperBound = arr.length - 1;
    var lowerBound = 0;
    let moveCount = 0;
    while (lowerBound <= upperBound) {
        var mid = Math.floor((upperBound + lowerBound) / 2);
        if (arr[mid] < target) {
            // iMid +- 1，可以讓指針持續移動，
            // 同時，也可以確保左右指針不會重複指向 iMid
            lowerBound = mid + 1;
        }
        else if (arr[mid] > target) {
            upperBound = mid - 1;
        }
        else {
            console.log(`moveCount:${moveCount}`)
            return mid;
        }

        moveCount += 1
    }

    console.log(`moveCount:${moveCount}`)
    return -1;
}

// 計算目標數連續出現的次數
// 注意，若要統計目標數不連續出現的次數，不需要經過排序->二分查找->前後遍歷
// 只需要遍歷所有元素就可以了
function continumCount(arr, target) {
    let numCount = 0

    let index = simpleBinarySearch(arr, target)
    console.log(`index:${index}`)

    if (index == -1) {
        // 沒有找到目標元素就直接返回 0
        return 0
    } else {
        // 二分法找到的位置也算一次
        numCount++

        // look-forward-loop，連續出現才會統計，不連續時迴圈就會停止
        for (var i = index - 1; arr[i] === target; i--) {
            numCount++
        }

        // look-backforward-loop，連續出現才會統計，不連續時迴圈就會停止
        for (var i = index + 1; arr[i] === target; i++) {
            numCount++
        }
    }

    return numCount
}

/* // 產生隨機數，並透過內建函數進行排列
// 注意，內建的排列函數若是沒有給定 cb 函數，有可能會排序錯誤
var d = new Dataset({ min: 0, max: 100, num: 10 })
d.arr.sort((a, b) => a - b)
console.log(d.arr)
console.log(myBinarySearch(d.arr, d.arr[8]))

d = new Dataset({ min: 0, max: 100, num: 10 })
d.arr.sort((a, b) => a - b)
console.log(d.arr)
console.log(simpleBinarySearch(d.arr, d.arr[8])) */

arr = [1, 3, 3, 3, 3, 5, 7, 9]
console.log(continumCount(arr, 3))

arr = [1, 3, 3, 5, 3, 3, 7, 3, 9]
console.log(continumCount(arr, 3))
