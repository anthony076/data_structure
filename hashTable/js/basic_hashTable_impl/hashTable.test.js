const test = require("tape")
const myHashTable = require("./hashTable")

const h = new myHashTable()

test("test get_hash", (t) => {
    t.is(h.hashFn("abc"), 35)
    t.end()
})

test("test set get ", (t) => {
    h.set("abc", 1)
    t.is(h.get("abc"), 1)

    t.ok(h.has("abc"))


    t.end()
})

test("test remove", (t) => {
    h.set("abc", 1)
    h.remove("abc")
    t.notOk(h.has("abc"))
    t.end()
})


test("test size", (t) => {
    t.equal(h.size(), 0)
    h.set("foo", 1)
    t.equal(h.size(), 1)

    t.end()
})

test("test clear", (t) => {
    h.clear()
    t.deepEqual(h.dataStore, {})

    t.end()
})

test("test values", (t) => {
    h.set("aa", 1); h.set("bb", 2); h.set("cc", 3)
    t.deepEqual(h.values(), [1, 2, 3])
    t.end()
})


test("test show", (t) => {
    t.equal(h.show(), "1,2,3")
    t.end()
})
