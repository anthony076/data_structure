/*
    注意，利用線性探測法 (Linear-Probes)，解決 hashValue 衝突的問題
*/

function NameValuePair(name, value) {
    this.name = name
    this.value = value
}

function myHashTable() {
    this.dataStore = {}

    this.hashFn = (name) => {
        let sum = null

        for (char of name) {
            sum += char.charCodeAt(0)
        }

        return sum % 37
    }

    this.has = (name) => {
        let hashValue = this.hashFn(name)

        // dataArr 未建立，表示沒有任何資料寫入
        if (this.dataStore[hashValue] == undefined) {
            return false
        }

        let dataArr = this.dataStore[hashValue]

        for (var i = 0; i < dataArr.length; ++i) {
            if (dataArr[i].name == name) {
                return true
            }
        }

        return false
    }

    this.set = (name, value) => {
        let found = false
        let hashValue = this.hashFn(name)

        if (this.dataStore[hashValue] == undefined) {
            // dataArr 未建立，表示沒有任何資料寫入
            this.dataStore[hashValue] = [new NameValuePair(name, value)]
        } else {
            let dataArr = this.dataStore[hashValue]

            // ==== 檢查資料在 dataArr 的位置 ====
            for (var i = 0; i < dataArr.length; ++i) {

                if (dataArr[i].name == name) {
                    // 資料已存在，覆寫原有資料
                    found = true
                    dataArr[i].value = value
                    break
                }
            }

            if (!found) {
                // 資料不存在，插入新資料進 dataArr
                dataArr.push(new NameValuePair(name, value))
            }
        }

    }

    this.get = (name) => {
        let found = false
        let hashValue = this.hashFn(name)

        let dataArr = this.dataStore[hashValue]
        for (var i = 0; i < dataArr.length; ++i) {
            if (dataArr[i].name == name) {
                found = true
                return dataArr[i].value
            }
        }

        if (!found) {
            return "value not found"
        }
    }

    this.remove = (name) => {
        let hashValue = this.hashFn(name)

        let dataArr = this.dataStore[hashValue]
        for (var i = 0; i < dataArr.length; ++i) {
            if (dataArr[i].name == name) {
                dataArr.splice(i, 1)
                break
            }
        }
    }

    this.size = () => {
        let count = 0

        for (hashValue in this.dataStore) {
            count += this.dataStore[hashValue].length
        }

        return count
    }

    this.clear = () => {
        this.dataStore = {}
    }

    this.values = () => {
        let result = [];

        for (hashValue in this.dataStore) {
            for (var i = 0; i < this.dataStore[hashValue].length; ++i) {
                key = this.dataStore[hashValue][i].name
                value = this.dataStore[hashValue][i].value
                result.push({ [key]: value })
            }
        }

        return result
    }
}

var h = new myHashTable();
h.set('Gandalf', 'gandalf@email.com');
h.set('John', 'johnsnow@email.com');
h.set('Tyrion', 'tyrion@email.com');
h.set('Aaron', 'aaron@email.com');
h.set('Donnie', 'donnie@email.com');
h.set('Ana', 'ana@email.com');
h.set('Jonathan', 'jonathan@email.com');
h.set('Jamie', 'jamie@email.com');
h.set('Sue', 'sue@email.com');
h.set('Mindy', 'mindy@email.com');
h.set('Paul', 'paul@email.com');
h.set('Nathan', 'nathan@email.com');

console.log(h.size())
console.log(h.values())

module.exports = myHashTable
