/*
    注意，此實現會有 hashValue 衝突的問題
*/

function myHashTable() {
    this.dataStore = {}

    this.hashFn = (name) => {
        let sum = null

        for (char of name) {
            sum += char.charCodeAt(0)
        }

        return sum % 37
    }

    this.has = (name) => {
        key = this.hashFn(name)
        return this.dataStore.hasOwnProperty(key)
    }

    this.set = (name, value) => {
        hashValue = this.hashFn(name)
        this.dataStore[hashValue] = value
    }

    this.get = (name) => {
        hashValue = this.hashFn(name)

        if (this.has(name)) {
            return this.dataStore[hashValue]
        } else {
            throw new Error("name not found")
        }
    }

    this.remove = (name) => {
        key = this.hashFn(name)
        delete this.dataStore[key]
    }

    this.size = () => {
        return Object.keys(this.dataStore).length
    }

    this.clear = () => {
        this.dataStore = {}
    }

    this.values = () => {
        return Object.values(this.dataStore)
    }

    this.show = () => {
        return Object.values(this.dataStore).join(",")
    }
}

var h = new myHashTable();
h.set('Gandalf', 'gandalf@email.com');
h.set('John', 'johnsnow@email.com');
h.set('Tyrion', 'tyrion@email.com');
h.set('Aaron', 'aaron@email.com');
h.set('Donnie', 'donnie@email.com');
h.set('Ana', 'ana@email.com');
h.set('Jonathan', 'jonathan@email.com');
h.set('Jamie', 'jamie@email.com');
h.set('Sue', 'sue@email.com');
h.set('Mindy', 'mindy@email.com');
h.set('Paul', 'paul@email.com');
h.set('Nathan', 'nathan@email.com');

// 因為 hash值得衝突，只有 正確寫入 7 個項目，括號中的鍵是被覆蓋的數據
// 16 - (Tyrion) Aaron
// 13 - (Donnie) Ana
// 5  - (Jonathan Jamie) Sue
// 32 - (Mindy) Paul
console.log(h.dataStore)

module.exports = myHashTable