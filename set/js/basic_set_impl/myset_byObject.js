/*
    注意，使用 object 實作 set 結構時，不要直接操作 this.dataStore
    因為 object 是無序的，因此，this.dataStore != this.values()
*/

// ==== set 結構的實現 ====
function mySet() {
    this.dataStore = {}

    this.has = (value) => {
        // 方法一，不推薦，this 會檢查原型鏈
        //return value in this.dataStore

        // 方法二，利用實例對象.hasOwnProperty()，不會檢查原型鏈
        return this.dataStore.hasOwnProperty(value)
    }

    this.add = (value) => {
        if (!this.has(value)) {
            this.dataStore[value] = value
        }
    }

    this.remove = (value) => {
        if (this.has(value)) {
            delete this.dataStore[value]
        } else {
            throw new Error("value not found")
        }
    }

    this.size = () => {
        return Object.keys(this.dataStore).length
    }

    this.clear = () => {
        this.dataStore = {}
    }

    this.values = () => {
        return Object.values(this.dataStore)
    }

    this.show = () => {
        return Object.values(this.dataStore).join(",")
    }

    // ==== 集合操作，並集，只要 A集合 或 B集合 有出現的成員，就放入新集合 ====
    // 需要透過 .add() 添加，而不是透過 .push() 添加，透過 .add() 才能保證數據的唯一性
    this.union = (otherSet) => {
        let unionSet = new mySet()

        // 將當前 mySet 實例中的成員，添加到新集合中
        // 注意，不要直接使用，this.dataStore[i]，使用 object 實作 set 結構時
        // 因為 object 是無序的，因此，this.dataStore != this.values()，
        var values = this.values()
        for (var i = 0; i < values.length; ++i) {
            unionSet.add(values[i])
        }

        var values = otherSet.values()
        // 將傳入的 mySet 實例中的成員，添加到新集合中
        for (var i = 0; i < values.length; ++i) {
            unionSet.add(values[i])
        }
        return unionSet
    }

    // ==== 集合操作，交集，A集合和B集合共用的成員，才放入新集合 ====
    this.intersect = (otherSet) => {
        let intersectSet = new mySet()

        for (var i = 0; i < otherSet.size(); ++i) {
            value = otherSet.values()[i]
            if (this.has(value)) {
                intersectSet.add(value)
            }
        }
        return intersectSet
    }

    // ==== 集合操作，或集，A集合 扣除 B集合共用的成員後，剩下的A集合成員 ====
    this.different = (otherSet) => {
        let differentSet = new mySet()

        // B集合帶入A集合中，在 A集合中找B集合中的差集成員
        for (var i = 0; i < this.size(); ++i) {
            value = this.values()[i]

            if (!otherSet.has(value)) {
                differentSet.add(value)
            }
        }

        return differentSet
    }

    // ==== 集合操作，子集，====
    // 條件1，子集的長度一定比較小
    // 條件2，子集中的成員，必定出現在另一個集合中
    this.isSubset = (otherSet) => {
        // 判斷條件1，子集的長度一定比較小
        if (otherSet.size() > this.size()) {
            return false
        }

        // 判斷條件2，子集中的成員，必定出現在另一個集合中
        for (var i = 0; i < otherSet.size(); ++i) {
            if (!this.has(otherSet.values()[i])) {
                return false
            }
        }

        return true
    }
}

module.exports = mySet