// ==== set 結構的實現 ====
function mySet() {
    this.dataStore = []

    this.has = (value) => {
        if (this.dataStore.indexOf(value) != -1) {
            return true
        } else {
            return false
        }
    }

    this.add = (value) => {
        if (!this.has(value)) {
            this.dataStore.push(value)
        }
    }

    this.remove = (value) => {
        index = this.dataStore.indexOf(value)

        if (index == -1) {
            throw new Error("value not found")
        } else {
            this.dataStore.splice(index, 1)
        }
    }

    this.size = () => {
        return this.dataStore.length
    }

    this.clear = () => {
        this.dataStore = []
    }

    this.values = () => {
        return this.dataStore
    }

    this.show = () => {
        return this.dataStore.join(",")
    }

    // ==== 集合操作，並集，只要 A集合 或 B集合 有出現的成員，就放入新集合 ====
    // 需要透過 .add() 添加，而不是透過 .push() 添加，透過 .add() 才能保證數據的唯一性
    this.union = (otherSet) => {
        let unionSet = new mySet()

        // 將當前 mySet 實例中的成員，添加到新集合中
        for (var i = 0; i < this.size(); ++i) {
            unionSet.add(this.dataStore[i])
        }

        // 將傳入的 mySet 實例中的成員，添加到新集合中
        for (var i = 0; i < otherSet.size(); ++i) {
            unionSet.add(otherSet.values()[i])
        }

        return unionSet
    }

    // ==== 集合操作，交集，A集合和B集合共用的成員，才放入新集合 ====
    this.intersect = (otherSet) => {
        let intersectSet = new mySet()

        for (var i = 0; i < otherSet.size(); ++i) {
            value = otherSet.values()[i]
            if (this.has(value)) {
                intersectSet.add(value)
            }
        }
        return intersectSet
    }

    // ==== 集合操作，或集，A集合 扣除 B集合共用的成員後，剩下的A集合成員 ====
    this.different = (otherSet) => {
        let differentSet = new mySet()

        // B集合帶入A集合中，在 A集合中找B集合中的差集成員
        for (var i = 0; i < this.size(); ++i) {
            value = this.dataStore[i]

            if (!otherSet.has(value)) {
                differentSet.add(value)
            }
        }

        return differentSet
    }

    // ==== 集合操作，子集，====
    // 條件1，子集的長度一定比較小
    // 條件2，子集中的成員，必定出現在另一個集合中
    this.isSubset = (otherSet) => {
        // 判斷條件1，子集的長度一定比較小
        if (otherSet.size() > this.size()) {
            return false
        }

        // 判斷條件2，子集中的成員，必定出現在另一個集合中
        for (var i = 0; i < otherSet.size(); ++i) {
            if (!this.has(otherSet.values()[i])) {
                return false
            }
        }

        return true
    }
}

module.exports = mySet