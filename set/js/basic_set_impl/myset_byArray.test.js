const test = require("tape")
const mySet = require("./myset_byArray.js")

let s = new mySet()

test("test add", (t) => {
    s.add(4)
    t.deepEqual(s.dataStore, [4])
    t.end()
})

test("test remove", (t) => {
    t.throws(() => s.remove(5), "value not found")

    s.remove(4)
    t.deepEqual(s.dataStore, [])
    t.end()
})

test("test size", (t) => {
    t.equal(s.size(), 0)
    s.add(1)
    t.equal(s.size(), 1)

    t.end()
})

test("test clear", (t) => {
    s.clear()
    t.deepEqual(s.dataStore, [])

    t.end()
})

test("test values", (t) => {
    s.add(2); s.add(1); s.add(3)
    t.deepEqual(s.values(), [2, 1, 3])
    t.end()
})

test("test show", (t) => {
    t.equal(s.show(), "2,1,3")
    t.end()
})

test("test has", (t) => {
    t.ok(s.has(1))
    t.notOk(s.has(4))
    t.end()
})

test("test union", (t) => {
    let a = new mySet()
    a.add("a"); a.add("b"); a.add("c"); a.add("d");

    let b = new mySet()
    b.add("e"); b.add("f"); b.add("g"); b.add("d");

    let unionSet = a.union(b)
    t.deepEqual(unionSet.values(), ["a", "b", "c", "d", "e", "f", "g"])

    t.end()
})

test("test intersect", (t) => {
    let a = new mySet()
    a.add("a"); a.add("b"); a.add("c"); a.add("d");

    let b = new mySet()
    b.add("b"); b.add("d"); b.add("g");

    let intersectSet = a.intersect(b)
    t.deepEqual(intersectSet.values(), ["b", "d"])

    t.end()
})

test("test different", (t) => {
    let a = new mySet()
    a.add("a"); a.add("b"); a.add("c"); a.add("d");

    let b = new mySet()
    b.add("b"); b.add("d"); b.add("g");

    let differentSet = a.different(b)
    t.deepEqual(differentSet.values(), ["a", "c"])

    t.end()
})

test("test isSubset", (t) => {
    let a = new mySet()
    a.add("a"); a.add("b"); a.add("c"); a.add("d");

    let b = new mySet()
    b.add("c"); b.add("a");

    let c = new mySet()
    c.add("c"); c.add("a"); c.add("e");

    t.ok(a.isSubset(b))
    t.notOk(a.isSubset(c))

    t.end()
})
