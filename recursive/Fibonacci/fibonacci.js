/*
f0 = 0  ，第0項
f1 = 1  ，第1項
fn = fn-1 + fn-2，第n項的值=前兩項值的和
fibonacci = 0,1,1,2,3,5,8,13,21,34 .....
    index   0,1,2,3,4,5,6, 7, 8
fibonacci(6) = 8， fibonacci 數列第6項為 8
fibonacci(8) = 21，fibonacci 數列第8項為 21
*/

// ==== 遞歸解 ====
function fibonacci(n) {
    if (n === 0) {
        return 0
    } else if (n == 1) {
        return 1
    }

    return fibonacci(n - 1) + fibonacci(n - 2)
}

console.log(fibonacci(6))   // 8
console.log(fibonacci(8))   // 21


// ==== 非遞歸解1，利用 for-loop ====
// 概念，從第0項開始，加到第n項
function noRecursive_forloop(n) {
    let fib_0 = 0, fib_1 = 1, fib_n;

    if (n === 0) {
        return fib_0
    } else if (n == 1) {
        return fib_1
    }

    // 初始化第0項和第1項
    // 迴圈從第2項開始，因為 f2=f0+f1
    item_a = fib_0
    item_b = fib_1

    for (var i = 2; i <= n; i++) {
        // 取得當前值 = 前兩項和
        fib_n = item_a + item_b

        // 計算完當前值後，將指標往後移動
        // ... item_a, item_b, fib_n ...
        // .........., item_a, item_b, fib_n...
        item_a = item_b
        item_b = fib_n
    }

    return fib_n
}

console.log(noRecursive_forloop(6))
console.log(noRecursive_forloop(8))

// ==== 非遞歸解2，利用 while-loop ====
// 概念，從第0項開始，加到第n項
function noRecursive_whileloop(n) {
    let fib_0 = 0, fib_1 = 1, fib_n;

    if (n === 0) {
        return fib_0
    } else if (n == 1) {
        return fib_1
    }

    // 初始化第0項和第1項
    // 迴圈從第2項開始，因為 f2=f0+f1
    item_a = fib_0
    item_b = fib_1

    let count = 0
    while (count <= n - 2) {
        fib_n = item_a + item_b
        item_a = item_b
        item_b = fib_n
        count++
    }

    return fib_n
}

console.log(noRecursive_whileloop(6))
console.log(noRecursive_whileloop(8))