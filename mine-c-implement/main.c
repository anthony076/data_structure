#include <stdio.h>
#include <string.h>
#include "utility/utility.h"
#include "bubble/bubble.h"
#include "select/select.h"
#include "insert/insert.h"
#include "shell/shell.h"
#include "merge/merge.h"

int main(void)
{
    // List Algorithm to user
    int choice;
    printf("Select Algorithm you want to execute:\n");
    printf("1. Bubble Algorithm\n");
    printf("2. Select Algorithm\n");
    printf("3. Insert Algorithm\n");
    printf("4. Shell Algorithm\n");
    printf("5. Merge Algorithm\n");
    printf("=======================\n");
    printf("Your choice is: ");
    scanf_s("%d", &choice);

    // 使用者輸入 array 長度
    int arrLength;
    printf("Enter array length:");
    scanf_s("%d", &arrLength);

    // 產生亂數
    int numArr[arrLength];
    getNumbs(numArr, arrLength);

    switch (choice)
    {
    case 1:
        printf("this is case1");
        // 冒泡排序
        bubble_sort_basic(numArr, arrLength);
        bubble_sort_optimize(numArr, arrLength);
        break;
    case 2:
        select_basic(numArr, arrLength);
        break;
    case 3:
        insert_basic(numArr, arrLength);
        break;
    case 4:
        shell_basic(numArr, arrLength);
        break;
    case 5:
        merge_basic(numArr, arrLength);
        break;
    default:
        printf("not avaliable value");
    }
}
