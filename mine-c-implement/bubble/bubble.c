#include <stdio.h>
#include "bubble.h"
#include "../utility/utility.h"

void bubble_sort_basic(int *numbers, int n)
{
    int count = 0;

    for (int i = 0; i < n; i++) // i，moving-times，
    {
        for (int j = 0; j < n - 1; j++) // j，moving-pointer,
        {

            if (numbers[j] > numbers[j + 1])
            {
                swap(&numbers[j], &numbers[j + 1]);
            }

            count++;
        }
    }

    printf("count=%d\n", count);
    print(numbers, n);
}

void bubble_sort_optimize(int *numbers, int n)
{
    int count = 0;

    // i 是遍歷的次數，透過倒序將內層遍歷的次數-1，用於時間上的優化
    for (int i = n; i > 0; i--) // i，moving-times，5,4,3,2,1
    {
        for (int j = 0; j < i - 1; j++) // j，moving-pointer,
        {
            if (numbers[j] > numbers[j + 1])
            {
                swap(&numbers[j], &numbers[j + 1]);
            }

            count++;
        }
    }

    printf("count=%d\n", count);
    print(numbers, n);
}
