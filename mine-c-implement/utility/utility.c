#include <time.h>
#include <stdlib.h>
#include <stdio.h>

void print(int *arr, int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("%d ", arr[i]);
    }

    printf("\n");
}

void getNumbs(int *arr, int n)
{
    int randNum;

    srand(time(NULL));

    for (int i = 0; i < n; i++)
    {
        randNum = (rand() % 100) + 1;
        arr[i] = randNum;
    }

    printf("init numbers:\n");
    print(arr, n);
    printf("\n");
}

void swap(int *a, int *b)
{
    int sum = *a + *b;
    *a = sum - *a;
    *b = sum - *b;
}

/*
int a = 1111;
    int b = 2222;
    swap(&a, &b);
    printf("a=%d, b=%d", a, b);
*/