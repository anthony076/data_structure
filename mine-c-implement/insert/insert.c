
#include <stdio.h>
#include "insert.h"
#include "../utility/utility.h"

void insert_basic(int *numbers, int n)
{
    int count = 0;
    for (int i = 1; i < n; i++) // i = 1,2,3,4,5，第一個元素不用比較
    {
        int curentValue = numbers[i];
        int j = i - 1; // 跟指針i的前一個元素比較

        while (curentValue < numbers[j] && j >= 0)
        //while (numbers[i] < numbers[j] && j >= 0)     // 錯誤，numbers[i] 是標的物且有可能被交換
        {
            swap(&numbers[j], &numbers[j + 1]);
            j--;
            count++;
        }
    }

    printf("count=%d\n", count);
    print(numbers, n);
}
