#include <stdio.h>
#include "../utility/utility.h"
#include "select.h"

void select_basic(int *numbers, int n)
{
    int imin = 0;
    int minValue;
    int count = 0;

    // i, moving-pointer, 0,1,2,3,4，最後一位不需要比較
    // 找到 最小值後，i 往右移動
    for (int i = 0; i < n - 1; i++)
    {
        // 預設右側第一位元素為最小值
        minValue = numbers[i];

        // j, find minimum pointer，
        // j 移動的過程中，若遇到更小值，就和第一位交換值
        for (int j = i; j < n; j++)
        {
            if (numbers[j] < minValue)
            {
                swap(&numbers[i], &numbers[j]);
                minValue = numbers[i]; // swap 之後要重新設定最小值
            }

            count++;
        }
    }

    printf("count=%d\n", count);
    print(numbers, n);
}