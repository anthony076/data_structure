
#include "shell.h"
#include <stdio.h>

void shell_basic(int *numbers, int n)
{
  int count = 0;
  for (int gap = 2; gap > 0; gap--)
  {
    for (int i = gap; i < n; i++)
    {
      int targetEl = numbers[i]; // targetEl 是右方元素

      for (int j = i - gap; j >= 0; j = j - gap) // 迴圈j遍歷結束後，新的j會從 i-gap 的位置開始
      {
        if (numbers[j] > targetEl) //如果左方的數值大，就將左右元素交換
        {
          swap(&numbers[j], &numbers[j + gap]);
          count++;
        }
      }
    }
  }

  printf("count=%d\n", count);
  print(numbers, n);
}
