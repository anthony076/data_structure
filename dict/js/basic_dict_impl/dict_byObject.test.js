const test = require("tape")
const myDict = require("./dict_byObject")

let s = new myDict()

test("test add", (t) => {
    s.add("foo", 4)
    t.deepEqual(s.dataStore, { "foo": 4 })
    t.end()
})

test("test remove", (t) => {
    t.throws(() => s.remove("bar"), "value not found")

    s.remove("foo")
    t.deepEqual(s.dataStore, {})
    t.end()
})

test("test size", (t) => {
    t.equal(s.size(), 0)
    s.add("foo", 1)
    t.equal(s.size(), 1)

    t.end()
})

test("test clear", (t) => {
    s.clear()
    t.deepEqual(s.dataStore, {})

    t.end()
})

test("test values", (t) => {
    s.add("aa", 1); s.add("bb", 2); s.add("cc", 3)
    t.deepEqual(s.values(), [1, 2, 3])
    t.end()
})

test("test show", (t) => {
    t.equal(s.show(), "1,2,3")
    t.end()
})

test("test has", (t) => {
    t.ok(s.has("aa"))
    t.notOk(s.has("dd"))
    t.end()
})

