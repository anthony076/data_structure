const test = require("tape")
const myDict = require("./dict_byArray")

let s = new myDict()

test("test add", (t) => {
    s.add("age", 1)
    t.deepEqual(s.values(), [1])
    t.end()
})

test("test remove", (t) => {
    s.remove("age")
    t.deepEqual(s.values(), [])
    t.end()
})

test("test size", (t) => {
    t.equal(s.size(), 0)
    s.add("foo", 2)
    t.equal(s.size(), 1)

    t.end()
})

test("test clear", (t) => {
    s.clear()
    t.deepEqual(s.values(), [])

    t.end()
})

test("test values", (t) => {
    s.add("foo", 2); s.add("bar", 1); s.add("aa", 3)
    t.deepEqual(s.values(), [2, 1, 3])
    t.end()
})

test("test show", (t) => {
    t.equal(s.show(), "2,1,3")
    t.end()
})

test("test has", (t) => {
    t.ok(s.has("foo"))
    t.notOk(s.has("666"))
    t.end()
})
