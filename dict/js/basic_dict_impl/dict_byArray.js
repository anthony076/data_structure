/*
    不推薦使用 array 來實現 dict 結構，

    #1，因為 array 部分內建函數，預設使用數字鍵
        使用自定義鍵的時候，某些內建函數就會失效，
        例如 arr.length、of 關鍵字、arr.indexOf()
    #2，雖然不能直接使用，但Object.values(arr) 轉成數字鍵後，
        可以間接使用，但使用起來不方便
*/

// ==== set 結構的實現 ====
function myDict() {
    this.dataStore = []

    /* set 結構 的寫法
    this.has = (value) => {
        if (this.dataStore.indexOf(value) != -1) {
            return true
        } else {
            return false
        }
    } */
    this.has = (value) => {
        if (Object.keys(this.dataStore).indexOf(value) != -1) {
            return true
        } else {
            return false
        }
    }

    /* set 結構 的寫法
    this.add = (value) => {
        if (!this.has(value)) {
            this.dataStore.push(value)
        }
    } */
    this.add = (key, value) => {
        if (!this.has(value)) {
            this.dataStore[key] = value
        }
    }

    /* set 結構 的寫法
    this.remove = (value) => {
        index = this.dataStore.indexOf(value)

        if (index == -1) {
            throw new Error("value not found")
        } else {
            this.dataStore.splice(index, 1)
        }
    } */
    this.remove = (key) => {
        delete this.dataStore[key]
    }

    /* set 結構 的寫法
    this.size = () => {
        return this.dataStore.length
    } */
    this.size = () => {
        return this.values().length
    }

    this.clear = () => {
        this.dataStore = []
    }

    /* set 結構 的寫法
    this.values = () => {
        return this.dataStore
    } */
    this.values = () => {
        return Object.values(this.dataStore)
    }

    /* set 結構 的寫法
    this.show = () => {
        return this.dataStore.join(",")
    } */
    this.show = () => {
        return this.values().join(",")
    }
}

module.exports = myDict















