
// ==== set 結構的實現 ====
function myDict() {
    this.dataStore = {}

    this.has = (value) => {
        // 方法一，不推薦，this 會檢查原型鏈
        //return value in this.dataStore

        // 方法二，利用實例對象.hasOwnProperty()，不會檢查原型鏈
        return this.dataStore.hasOwnProperty(value)
    }

    /* set 結構的寫法
    this.add = (value) => {
        if (!this.has(value)) {
            this.dataStore[value] = value
        }
    }
    */
    this.add = (key, value) => {
        if (!this.has(value)) {
            this.dataStore[key] = value
        }
    }

    /* set 結構的寫法
    this.remove = (value) => {
        if (this.has(value)) {
            delete this.dataStore[value]
        } else {
            throw new Error("value not found")
        }
    } */
    this.remove = (key) => {
        if (this.has(key)) {
            delete this.dataStore[key]
        } else {
            throw new Error("value not found")
        }
    }

    this.size = () => {
        return Object.keys(this.dataStore).length
    }

    this.clear = () => {
        this.dataStore = {}
    }

    this.values = () => {
        return Object.values(this.dataStore)
    }

    this.show = () => {
        return Object.values(this.dataStore).join(",")
    }

}

module.exports = myDict