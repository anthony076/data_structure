
function Graph() {
    // 頂點列表
    this.vertices = [];

    // 利用 objct 結構建立頂點連接表，
    this.wiredTable = {}

    // 建立新頂點
    this.addVertex = function (vertex) {
        // 將傳入頂點加入頂點列表中
        this.vertices.push(vertex);
        // 為傳入頂點在連接表中初始化儲存空間
        this.wiredTable[vertex] = []
    }

    // 建立邊，注意，頂點必須預先建立後才能加入邊
    this.addEdge = function (vertex, wiredVertex) {
        // 將連接頂點加入頂點的列表中
        this.wiredTable[vertex].push(wiredVertex)
        // 將頂點加入連接頂點的列表中
        this.wiredTable[wiredVertex].push(vertex)
    }

    // 印出當前 this.wiredTable 的內容
    this.toString = function () {
        for (key in this.wiredTable) {
            let result = key + " -> "

            for (wiredVertex of this.wiredTable[key]) {
                result += wiredVertex + " "
            }
            console.log(result)
        }
    }

    // ==== 廣度優先遍歷法，Breadth-First-Search，BFS ====
    // 初始化 visited-array，用來記錄頂點是否已經訪問過的 visited-array
    this.initVisitedArr = function () {
        var visited = [];

        for (var i = 0; i < this.vertices.length; i++) {
            visited[this.vertices[i]] = false
        }

        return visited;
    }

    this.bfs = function () {
        var histroy = ""
        var visited = this.initVisitedArr()

        // 用 array 模擬 queue 的數據結構
        // js array 有提供 模擬 queue 數據結構的 api
        // 由右到左的 queue，array.push()尾插入 / array.shift()頭取出
        // 由左到右的 queue，array.unshift()頭插入 / array.pop()尾取出
        var q = []
        q.push(this.vertices[0])

        // 從 q 中取出頂點，並將該頂點的連接頂點加入 queue 中
        while (q.length != 0) {

            // 從 q 中取出頂點
            let curVertex = q.shift()

            // 將當前頂點的狀態設置為true(已訪問)
            visited[curVertex] = true

            for (WiredVertex of this.wiredTable[curVertex]) {
                if (visited[WiredVertex] === false) {
                    // 若當前的連接頂點尚未訪問過，就將其狀態改為true(已訪問)
                    visited[WiredVertex] = true

                    //更改狀態後，再將當前連接頂點添加到 queue 中
                    q.push(WiredVertex)
                }
            }
            histroy += curVertex + " "
        }
        console.log(histroy)
    }

    // ==== 深度優先遍歷法，Depth-First-Search，DFS ====
    this.dfsRecursive = function (vertex, visited) {
        console.log(`${vertex}`)

        visited[vertex] = true
        for (var wiredVertex of this.wiredTable[vertex]) {
            if (visited[wiredVertex] == false) {
                this.dfsRecursive(wiredVertex, visited)
            }
        }
    }

    this.dfs = function () {
        // 初始化訪問表
        var visited = this.initVisitedArr()

        // 取得第一個頂點，並將其訪問狀態設置為 true(已訪問)
        let firstVertex = this.vertices[0]
        visited[firstVertex] = true

        console.log(`${firstVertex}`)

        // 透過遞歸進行深度優先的遍歷
        for (var wiredVertex of this.wiredTable[firstVertex]) {
            if (visited[wiredVertex] === false) {
                this.dfsRecursive(wiredVertex, visited)
            }
        }
    }
}

let g = new Graph()

// 建立頂點
var myVertices = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];
for (vertex of myVertices) {
    g.addVertex(vertex)
}

// 建立邊
g.addEdge('A', 'B');
g.addEdge('A', 'C');
g.addEdge('A', 'D');
g.addEdge('C', 'D');
g.addEdge('C', 'G');
g.addEdge('D', 'G');
g.addEdge('D', 'H');
g.addEdge('B', 'E');
g.addEdge('B', 'F');
g.addEdge('E', 'I');
//g.toString()

//g.bfs()  // A B C D E F G H I
g.dfs()     // A B E I F C D G H


