/*
    v:vertex 當前頂點
    w:wired-vertex 連接頂點
*/

function Graph(num) {
    // num 代表頂點總數
    this.totalVertices = num
    // 邊的數量
    this.edges = 0
    // ==== 建立空連接表 ====
    // this.adj 表示每個頂點相連接的其他頂點的對應
    this.adj = []

    // 將 this.adj 擴充成 2d-array，[[],[],[],[],[],]
    for (var i = 0; i < this.totalVertices; ++i) {
        this.adj[i] = []
        this.adj[i].push(i.toString())
    }

    // ==== 將所有頂點都標記為尚未訪問過 ====
    // false，未被訪問過 / true，已經訪問過
    this.marked = []
    for (var i = 0; i < this.totalVertices; ++i)
        this.marked[i] = false

    // ==== 添加邊 (添加新頂點) ====
    // v: 頂點1, w: 頂點2 (一個邊會有兩個頂點)
    this.addEdge = function (v, w) {
        // 在 v頂點的連接表中加入 w頂點
        this.adj[v].push(w)
        // 在 w頂點的連接表中加入 v頂點
        this.adj[w].push(v)
        ++this.edges
    }

    // ==== 印出當前 graph 的結構 ====
    // 注意，this.vertices 傳入的是數字
    this.showGraph = function () {
        for (var i = 0; i < this.totalVertices; ++i) {
            let result = `${i} -> `

            for (var j = 0; j < this.totalVertices; ++j) {
                if (this.adj[i][j + 1] != undefined) {
                    result += `${this.adj[i][j + 1]} `
                }
            }
            console.log(result)
        }
    }

    // ==== 深度優先遍歷 dfs ====
    this.dfs = function (v) {
        /* 每個連接表的第一個元素是 Label，代表是哪一個頂點的連接表
        [ [ '0', 1, 2 ],
        [ '1', 0, 3 ],
        [ '2', 0, 4 ],
        [ '3', 1 ],
        [ '4', 2 ] ]
        透過遞歸，讀取節點時，會先檢查該頂點的連接表是否已經讀取過，若否，先跳到該頂點取執行
        利用遞歸實現深度的效果
        */
        console.log("==========")
        console.log(v)

        // 將當前頂點設置為已訪問
        this.marked[v] = true
        //console.log(this.marked)

        // 取得當前頂點的連接表
        for (var w of this.adj[v]) {
            //1，console.log(w) // w 依序為 "0" 1 "1" 0 3 "3" 1 2 "2" 0 4 "4" 2
            //2，當 w="1" 的時候， 會先去取 this.adj["1"] 的連接表，"1" -> 0 3
            //3，若沒有加入 this.marked[w]，會進入無限循環，
            //   因為頂點會共用邊，例如，0 連接到 1，等同於 1 連接到 0，
            //   因此使用遞歸時，需要 marked 避免 無限循環
            if (!this.marked[w]) {
                this.dfs(w)
            }
        }
    }

    // ==== 廣度優先遍歷 bfs ====
    this.bfs = function (s) {
        // 利用 queue 實現廣度優先
        // 注意，js 的 array 可以當 queue 使用，但要使用 queue 的 api
        var queue = []

        // 將當前頂點標註為已訪問
        this.marked[s] = true

        // 插入第一個遍歷點
        queue.push(s)

        while (queue.length > 0) {
            // 從 queue 的頭部，取出節點
            var v = queue.shift()
            console.log(`v: ${v}`)

            // 將當前頂點的所有連接頂點推入 queue 尾部
            // 若已訪問過的就不將當前的連接頂點推入 queue 尾部
            // this.adj["0"] = 1 2
            for (var w of this.adj[v]) {
                // w 為當前頂點的連結頂點

                if (!this.marked[w]) {
                    // 將當前的連接頂點設置為已訪問
                    this.marked[w] = true
                    // 將當前的連接頂點加入 queue 中
                    queue.push(w)
                }
            }
        }
    }

    // ==== 最短路徑的實現 ====
    // this.edgeTo 上層頂點表
    // 利用 bfs 實現最短路徑的計算，但添加 this.edgeTo 來記錄每個頂點的上一層頂點名
    this.edgeTo = []

    this.shortestPath = function (searchVertex) {
        rootVertex = 0
        // 利用 queue 實現廣度優先
        // 注意，js 的 array 可以當 queue 使用，但要使用 queue 的 api
        var queue = []

        // 將當前頂點標註為已訪問
        this.marked[rootVertex] = true

        // 插入第一個遍歷點
        queue.push(rootVertex)

        // ==== step1，利用廣度優先法建立逆向連接表 ====
        // 廣度優先法，詳細請見 this.bfs()
        while (queue.length > 0) {
            var curVertex = queue.shift()

            for (var wiredVertex of this.adj[curVertex]) {
                if (!this.marked[wiredVertex]) {
                    // thi.edgeTo 用來記錄當前連結頂點的上一層頂點
                    // w:0 -> 根頂點，沒有上一層，v:undefined
                    // 連接頂點w為1 -> 上一層頂點v為0
                    // 連接頂點w為2 -> 上一層頂點v為0
                    // 連接頂點w為3 -> 上一層頂點v為0
                    // 連接頂點w為2 -> 上一層頂點v為1
                    // 所以 this.edgeTo = [undefined, 0, 0, 0, 1, 1, 2, 3, 4]
                    this.edgeTo[wiredVertex] = curVertex

                    // 將當前連接的頂點設置為已訪問
                    this.marked[wiredVertex] = true
                    // 將當前的連接頂點加入 queue 中
                    queue.push(wiredVertex)
                }
            }
        }

        // 建立好的逆向連接表
        //console.log(this.edgeTo)

        // ==== step2，利用逆向連接表查詢最短路徑 ====
        // 利用廣度優先法建立的逆向連接表
        // this.edgeTo = [ null, 0, 0, 0, 1, 1, 2, 3, 4]

        let path = []
        for (let i = searchVertex; i >= rootVertex; i = this.edgeTo[i]) {
            path.push(i)
        }
        //console.log(path)   // [7, 3, 0]

        // ==== step3，印出結果 ====
        console.log(path.reverse().join("-"))
    }

    // ==== top-sort 的實現 ====
    this.topSortHelper = function (v, visited, stack) {
        console.log("===========")
        visited[v] = true;
        console.log(`v: ${v}`)
        for (var w in this.adj[v]) {
            if (!visited[w]) {
                console.log(`w: ${w}`)
                this.topSortHelper(w, visited, stack);
            }
        }
        stack.push(v);
        console.log(`stack: ${stack}`)
    }

    this.topoSort = function (list) {
        var stack = [];
        var visited = [];   // 已訪問列表

        // 將已訪問列表初始化為 false
        for (var i = 0; i < this.totalVertices; i++) {
            visited[i] = false;
        }

        //
        for (var i = 0; i < this.totalVertices; i++) {
            if (visited[i] == false) {
                this.topSortHelper(i, visited, stack);
            }
        }

        for (var i = 0; i < stack.length; i++) {
            if (stack[i] != undefined && stack[i] != false) {
                this.adj[stack[i]]
            }
        }
    }
}

let g = new Graph(9)
g.addEdge(0, 1)
g.addEdge(0, 2)
g.addEdge(0, 3)
g.addEdge(1, 4)
g.addEdge(1, 5)
g.addEdge(2, 3)
g.addEdge(2, 6)
g.addEdge(3, 6)
g.addEdge(3, 7)
g.addEdge(4, 8)
g.showGraph()

// 深度優先遍歷法
//g.dfs(0)        // 0 -> 1 -> 3 -> 2 -> 4
//g.dfs(0)        // 0 -> 1 -> 4 -> 8 -> 5 -> 2 -> 3 -> 6 -> 7

// 廣度優先遍歷法
//g.bfs(0)          // 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8

// 最短路徑尋找法
g.shortestPath(7)   // 0-3-7




