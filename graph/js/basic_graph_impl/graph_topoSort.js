
/*  概念，利用深度優先遍歷法(dfs) 的 拓樸排序
    step1，透過 dfs 的遞歸方式，移動到頂點末端(沒有其他可遍歷的連接頂點, 出度為0)
        # 新節點優先移動
        # 已訪問過的節點略過不處理
    step2，當前頂點移動到末端後(末端指的是沒有其他可遍歷的連接頂點)，
        # 將當前頂點推入 stack 中，
        # 回到上一層函數
*/

function Graph(v) {
    this.vertices = v;
    this.edges = 0;
    this.vertexList = [];
    this.wiredVertex = [];

    // 將 wiredVertex 擴充為 2d-array
    for (var i = 0; i < this.vertices; i++) {
        this.wiredVertex[i] = [];
    }

    // 添加新頂點
    this.addEdge = function (v, w) {
        this.wiredVertex[v].push(w);
        this.wiredVertex[w].push(v);
        this.edges++;
    }

    // 透過 dfs，讀取當前頂點的連接頂點，
    //      若為新頂點，透過遞歸跳到新頂點
    //      反之，沒有其他可遍歷的連接頂點，將當前頂點推入 stack 中，返回到上一層函數
    this.topSortHelper = function (curVertex, visited, stack) {
        // 頂點訪問順序 0->1->2->4->5->3 (新節點優先)
        console.log("=================")
        console.log("current vertex: " + curVertex);

        visited[curVertex] = true;
        for (var w of this.wiredVertex[curVertex]) {
            if (!visited[w]) {
                console.log(`new wired-vertex: ${w}`)
                this.topSortHelper(w, visited, stack);
            } else {
                console.log(`visited wired-vertex: ${w}`)
            }
        }

        // 當前頂點是末端(沒有新頂點)後，結束 for迴圈，
        // 離開前，將當前頂點推入 stack 中
        // 頂點推入 stack 順序，4->5->2->3->1->0
        stack.push(curVertex);

        console.log("current stack: " + stack)
    }

    this.topoSort = function () {
        var stack = [];     // 拓樸排序的結果
        var visited = [];    // 標記頂點是否訪問過

        // 將 visited 初始化
        for (var i = 0; i < this.vertices; i++) {
            visited[i] = false;
        }

        // 利用 dfs 走到頂點最末端 (有新頂點就優先跳到新頂點)
        // 走到末端後，紀錄當前頂點，
        for (var i = 0; i < this.vertices; i++) {
            if (!visited[i]) {
                // 當前頂點尚未訪問過
                this.topSortHelper(i, visited, stack);
            }
        }

        // 印出 stack 中的結果
        // vertexList   = [ a, b, c, d, e, f ]
        // stack        = [ 4, 5, 2, 3, 1, 0 ] = [e, f, c, d, b, a]
        while (stack.length > 0) {
            console.log(this.vertexList[stack.pop()]);
        }
    }
}

g = new Graph(6);
g.addEdge(0, 1);
g.addEdge(1, 2);
g.addEdge(1, 3);
g.addEdge(2, 4);
g.addEdge(2, 5);
g.vertexList = ["a", "b", "c", "d", "e", "f"];

g.topoSort();    // 印出 e, f, c, d, b, a