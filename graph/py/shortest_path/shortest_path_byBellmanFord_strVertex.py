
def bellman_ford(graph, source):
    distance = {}

    max = 99
    for v in graph:
        distance[v] = max  #赋值为负无穷完成初始化
    distance[source] = 0

    # 外層迴圈，遍歷所有邊
    for i in range(len( graph ) - 1):
        # 中層迴圈，遍歷所有頂點
        for vertex in graph:
            # 底層迴圈，遍歷所有連接頂點
            for wiredVertex in graph[vertex]:
                if distance[wiredVertex] > graph[vertex][wiredVertex] + distance[vertex]:
                    distance[wiredVertex] = graph[vertex][wiredVertex] + distance[vertex]

    #判断是否存在环路
    for vertex in graph:
        for wiredVertex in graph[vertex]:
            if distance[wiredVertex] > distance[vertex] + graph[vertex][wiredVertex]:
                return None

    return distance

graph = {
    'a': {'b': 6, 'c':  5, 'd':5},
    'b': {'e':  -1},
    'c': {'b':-2, 'e':1},
    'd': {'c':  -2, 'f':-1},
    'e': {'g': 3},
    'f': {'g': 3},
    'g': {}
}

distance = bellman_ford(graph, 'a')
print(distance)

