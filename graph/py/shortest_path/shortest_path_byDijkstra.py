import numpy as np

def dijkstra(graph):
	n = 6
	m = 9
	inf = 99

	# e 為2d連接表
	e = np.zeros((n+1,n+1), dtype=int)

	# ==== 初始化2d連接表 ====
	for i in range(1,n+1):
		for j in range(1,n+1):
			if i==j:
				e[i][j] = 0
			else:
				e[i][j] = inf

	#  根據 graph 的內容將數值填入連接表中
	for line in graph:
		y, x, w = map(int, line.split(" "))
		e[y][x] = w
	print("e:\n", e)

	# ==== init dis-array ====
	# dis-array，最短距離表
	sorceVertex = 1
	shortest = np.zeros(n+1,dtype=int)
	for i in range(1,n+1):
		shortest[i] = e[sorceVertex][i]

	# init visited-array，用來紀錄頂點是否已經是最短路徑的頂點
	# 	0，代表該頂點還不是最短路徑，
	# 	1，代表該頂點已經是最短路徑，
	visited = np.zeros(n+1,dtype=int)
	visited[sorceVertex] = 1	# 設定原點一開始就已經是最短路徑

	for i in range(1, n+1):
		print("="*30)
		# ==== 找到離 sourceVertex 最近的連接頂點 ====
		min = inf
		for wiredVertex in range(1, n+1):
			# 條件1，當頂點還不是最短路徑(visited[wiredVertex] == 0)，
			# 條件2，且當前頂點對應的權重和 < int (shortest[wiredVertex] < min)
			# 條件1 和 條件2 都成立時，代表當前頂前已經是 U 集合中的最短路徑
			if visited[wiredVertex] == 0 and shortest[wiredVertex] < min:
				min = shortest[wiredVertex]
				newVertex = wiredVertex
				print("shortest vertex is : ", wiredVertex)

		# ==== 將當前最近連接頂點，設置為已經是最短路徑 ====
		visited[newVertex] = 1

		# ==== 利用目前的最小連接頂點，更新其他未成為最短路徑的連接頂點 ====
		for v in range(1, n+1):
			newValue = shortest[newVertex] + e[newVertex][v]
			if shortest[v] > newValue:
				shortest[v] = newValue
				print("update vetex:{} to {}".format(v, newValue) )

		print("shortest: ", shortest)
		print("visited : ", visited)

	return shortest

if __name__ == "__main__":

	graph = [
		"1 2 1",
		"1 3 12",
		"2 3 9",
		"2 4 3",
		"3 5 5",
		"4 3 4",
		"4 5 13",
		"4 6 15",
		"5 6 4",
	]

	dijkstra(graph)


