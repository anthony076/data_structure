import numpy as np
import pandas as pd

inf = 100

n=4 # 頂點的數量
m=8 # 邊的數量
graph = [
    # 起點 終點 權重
    "1 2 2",
    "1 4 4",
    "4 1 5",
    "1 3 6",
    "3 1 7",
    "3 4 1",
    "4 3 12",
    "2 3 3"
]

#initialize
e = np.zeros((5,5), dtype = np.int)

for i in range(1, n+1):
	for j in range(1, n+1):
		if i==j:
			e[i][j]=0
		else:
			e[i][j]=inf

#read line
for i in range(0, m):
	t1, t2, t3 = map(int, graph[i].split(' '))
	e[t1][t2]=t3

#Floyd-Warshall algorithm
for k in range(1, n+1):
	for i in range(1, n+1):
		for j in range(1, n+1):
			print("=============")
			print("{}, {}, {}".format(i,j,k))
			if e[i][j] > e[i][k]+e[k][j]:
				print("{} -> {}".format(e[i][j], e[i][k]+e[k][j]))
				e[i][j] = e[i][k]+e[k][j]

#output final consequences
for i in range(1, n+1):
    print(e[i][1:n+1])

