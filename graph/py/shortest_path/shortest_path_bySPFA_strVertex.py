
import queue

def bellman_ford(graph, source):
    distance = {}

    max = 99
    for v in graph:
        distance[v] = max  #赋值为负无穷完成初始化
    distance[source] = 0

    # 外層迴圈，遍歷所有邊
    for i in range(len( graph ) - 1):
        # 中層迴圈，遍歷所有頂點
        for vertex in graph:
            # 底層迴圈，遍歷所有連接頂點
            for wiredVertex in graph[vertex]:
                if distance[wiredVertex] > graph[vertex][wiredVertex] + distance[vertex]:
                    distance[wiredVertex] = graph[vertex][wiredVertex] + distance[vertex]

    #判断是否存在环路
    for vertex in graph:
        for wiredVertex in graph[vertex]:
            if distance[wiredVertex] > distance[vertex] + graph[vertex][wiredVertex]:
                return None

    return distance

def spfa(graph, source):
    max = 99

    distance = {}
    visted = {}
    q = queue.Queue()

    # 初始化用到的表
    for vertex in graph:
        distance[vertex] = max  #distance:{a:99, b:99, c:99, ....}

        # 將頂點的訪問紀錄初始化為 False，未訪問
        visted[vertex] = False  #visted:{a:False, b:False, c:False, ....}

    # 將原點到原點的距離設置為 0
    distance[source] = 0

    # 將第一個原點加入 queue 中
    q.put(source)
    visted[source] = True

    while not q.empty():
        print("="*10)

        # 取出 queue 中的第一個頂點
        vertex = q.get()
        print("current Vertex: ",vertex)

        # 遍歷當前頂點的所有連接邊
        for wiredVertex in graph[vertex]:
            print("current wiredVertex: ",wiredVertex)

            # 當前後繼頂點到原點的位置
            curWeight =  distance[wiredVertex]
            # 後繼頂點的新權重和 = 前驅頂點到原點的位置 + 當前邊的權重
            newWeight = distance[vertex] + graph[vertex][wiredVertex]

            if curWeight > newWeight:
                print("update")
                print("curWeight: ", curWeight)
                print("newWeight: ", newWeight)
                distance[wiredVertex] = distance[vertex] + graph[vertex][wiredVertex]

                # 更新成功的後繼頂點，才會被加入 queue 中
                if[visted[wiredVertex] == False]:
                    q.put(wiredVertex)
                    visted[wiredVertex] = True

                print("distance: ", distance)
                print("-"*5)

    return distance

if __name__ == "__main__":
    graph = {
        'a': {'b': 6, 'c':  5, 'd':5},
        'b': {'e':  -1},
        'c': {'b':-2, 'e':1},
        'd': {'c':  -2, 'f':-1},
        'e': {'g': 3},
        'f': {'g': 3},
        'g': {}
    }

    #distance = bellman_ford(graph, 'a')    # {'a': 0, 'b': 1, 'c': 3, 'd': 5, 'e': 0, 'f': 4, 'g': 3}
    distance = spfa(graph, 'a')             # {'a': 0, 'b': 1, 'c': 3, 'd': 5, 'e': 0, 'f': 4, 'g': 3}

    print(distance)



