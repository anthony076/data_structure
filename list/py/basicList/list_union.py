"""
功能
    找出兩個 list 都有的元素集合，重複元素只取一次

思路，
    #1  檢查短的array，
    #2  對短array中的元素進行查找，
        > 若元素不存在長array中，插入到長array
        > 若元素存在長array中，pass
"""

from typing import List, Any
def union(a:List[Any], b:List[Any]) -> List[Any]:

    long_list = None
    short_list = None

    # ==== find long_list & short_list ====
    len_a = len(a)
    len_b = len(b)

    if len_a >= len_b:
        short_list = b
        long_list = a
    else:
        short_list = a
        long_list = b

    # 查找元素是否重複，若否，插入新元素，若是，pass
    for item in short_list:
        index = None
        try:
            index = long_list.index(item)
        except:
            pass
            
        if index == None:
            long_list.append(item)   

    return long_list

if __name__ == "__main__":
    print(union([1,2,3], [4,5]))
