import pytest, random
from pylist import MyList, union

@pytest.fixture
def mylist():
    return MyList()

class Test_MyList:

    def test_empty_init(self, mylist):
        assert mylist.mylist == list()

    def test_init(self):
        mylist = MyList(4,5,6)
        assert mylist.mylist == [4,5,6]

    def test_set_mylist_usage(self, mylist):
        mylist.set_mylist = [4,5,6]
        assert mylist.mylist == [4,5,6]

    def test_is_empty(self, mylist):
        assert mylist.is_empty() == True

        mylist = MyList(4,5,6)
        assert mylist.is_empty() == False

    def test_clear(self):
        mylist = MyList(4,5,6)
        assert mylist.is_empty() == False

    def test_getLength(self):
        mylist = MyList(4,5,6)
        assert mylist.getLength() == 3

    def test_getItem_usage(self, mylist):
        mylist.set_mylist = [4, 5, 6]
        assert mylist.getItem(1) == 4

    def test_getItem_error(self, mylist):
        mylist.set_mylist = [4, 5, 6]

        with pytest.raises(AssertionError, match="index must start from 1"):
            mylist.getItem(0)
        
        with pytest.raises(AssertionError, match="index must start from 1"):
            mylist.getItem(-1)

        with pytest.raises(AssertionError, match="out of index range"):
            mylist.getItem(4)

        with pytest.raises(AssertionError, match="wrong index type, must be a int object"):
            mylist.getItem("2")

    def test_insertItem_usage(self, mylist):
        mylist.set_mylist = [1,2,3]
        assert mylist.insertItem(1,4) == [1,4,2,3]

    def test_insertItem_usage(self, mylist):
        mylist.set_mylist = [1,2,3]
        assert mylist.insertItem(2,4) == [1,4,2,3]

    def test_insertItem_error(self, mylist):
        mylist.set_mylist = [4, 5, 6]

        with pytest.raises(AssertionError, match="index must start from 1"):
            mylist.insertItem(0, 4)
        
        with pytest.raises(AssertionError, match="index must start from 1"):
            mylist.insertItem(-1, 4)

        with pytest.raises(AssertionError, match="out of index range"):
            mylist.insertItem(4, 4)

        with pytest.raises(AssertionError, match="wrong index type, must be a int object"):
            mylist.insertItem("2", 4)

    def test_deleteItem_usage(self, mylist):
        mylist.set_mylist = [1, 2, 3]
        assert mylist.deleteItem(2) == [1, 3]

    def test_deleteItem_error(self, mylist):
        mylist.set_mylist = [4, 5, 6]

        with pytest.raises(AssertionError, match="index must start from 1"):
            mylist.deleteItem(0)
        
        with pytest.raises(AssertionError, match="index must start from 1"):
            mylist.deleteItem(-1)

        with pytest.raises(AssertionError, match="out of index range"):
            mylist.deleteItem(4)

        with pytest.raises(AssertionError, match="wrong index type, must be a int object"):
            mylist.deleteItem("2")

    def test_getIndex_usage(self, mylist):
        mylist.set_mylist = [11, 12, 13]
        assert mylist.getIndex(13) == 3 
        assert mylist.getIndex(14) == None

def test_union():
    a = MyList(1, 2, 3)
    b = MyList(2, 4)

    assert union(a, b).mylist == [1, 2, 3, 4]

    a = MyList(1, 2, 3)
    b = MyList(4, 5)

    assert union(a, b).mylist == [1, 2, 3, 4, 5]

    a = MyList(1, 2, 3)
    b = MyList(1, 2, 3)

    assert union(a, b).mylist == [1, 2, 3]

    a = MyList(1, 2, 3)
    b = MyList(4, 5, 6)

    assert union(a, b).mylist == [1, 2, 3, 4, 5, 6]
