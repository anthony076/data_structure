from list_union import union

def test_union():
    a = [1, 2, 3]
    b = [2, 4]

    assert union(a, b) == [1, 2, 3, 4]

    a = [1, 2, 3]
    b = [4, 5]

    assert union(a, b) == [1, 2, 3, 4, 5]

    a = [1, 2, 3]
    b = [1, 2, 3]

    assert union(a, b) == [1, 2, 3]

    a = [1, 2, 3]
    b = [4, 5, 6]

    assert union(a, b) == [1, 2, 3, 4, 5, 6]