import pytest

from list_delete import delete_element

def test_delete_element_usage():
    a = [1,2,3]
    assert delete_element(a, 0) == [2,3]

    a = [1,2,3]
    assert delete_element(a, 1) == [1,3]

    a = [1,2,3]
    assert delete_element(a, 2) == [1,2]

def test_delete_element_error():
    with pytest.raises(AssertionError, match="index is out of range"):
        delete_element([1,2,3], 3)

    with pytest.raises(AssertionError, match="List is empty"):
        delete_element([], 0)


