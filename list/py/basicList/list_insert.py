"""
功能
    對於一個具有固定大小的 list，在 list 未滿的情況下，
    將指定的元素(ele)插入指定的位置(index)後，返回完成插入後的 list

py的限制，
    插入元素後，list 的長度會增加，list 只能透過 .append() 或 .insert() 的方式增加長度
    因此處理前，利用 .append() 插入 None，用來增加 list 的長度

思路
    @Action
        將元素插入，
        指定位置以後的元素往後移

    @Flow
        #1  檢查輸入參數，設定邊界條件
        #2  index 以後的元素往後移
            從最後一個元素開始，每次位置(i) -1，到 index +1 停止
        #3  將指定元素插入指定位置
"""
from typing import List, Any
from array import array

def insert_element(a:List[Any], index:int, element:Any) -> List:
    MAX_SIZE = 5

    # ==== check List length =====
    if len(a) >= MAX_SIZE:
        assert False, "List is full, the MAX_SIZE of List is 5"
    # ==== check index =====
    if index >= len(a) :
        assert False, "index is out of range"

    # ==== Flow ====
    # step1，增加 List 的長度
    a.append(None)

    # step2，將 index 位置以後的元素，向右位移一位
    for i in range(len(a)-1, index, -1):
        a[i] = a[i-1]
    
    # step3，將元素插入指定位置
    a[index] = element

    return a

def array_example():        
    # https://docs.python.org/3/library/array.html
    arr = array("i", [1,2,3])
    arr.append(4)

    print(arr.buffer_info())
    print(arr.tolist())

if __name__ == "__main__":
    pass