"""
class MyList
    #1 index must start from 1
"""

from typing import List, Any, Optional

# 自定義的線性表
class MyList:
    def __init__(self, *arg):
        if arg == ():
            self.__myList = list()
        else:
            self.set_mylist = [item for item in arg]

    @property
    def mylist(self):
        return self.__myList

    @mylist.setter  # type:ignore
    def set_mylist(self, value:List[Any]):
        self.__myList = value

    def is_empty(self) -> bool:
        if len(self.__myList) > 0:
            return False
        else:
            return True

    def clear(self) -> None:
        self.__myList = []

    def getIndex(self, value:Any) -> Optional[int]:
        result = None

        for index, element in enumerate(self.__myList):
            if element == value:
                result = index + 1
                return result

        return result

    def getItem(self, index:int) -> Any:
        self._checkIndex(index)

        return self.__myList[index-1]

    def getLength(self) -> int:
        return len(self.__myList)

    def insertItem(self, index:int, value:int) -> List[Any]:
        self._checkIndex(index)

        # step1，增加 List 的長度
        self.__myList.append(None)

        # step2，將 index 位置以後的元素，向右位移一位
        for i in range(len(self.__myList)-1, index-1, -1):
            self.__myList[i] = self.__myList[i-1]
        
        # step3，將元素插入指定位置
        self.__myList[index-1] = value

        return self.__myList

    def deleteItem(self, index:int) -> List[Any]:
        self._checkIndex(index)

        # ==== check input list ====
        if len(self.__myList) == 0:
            assert False, "List is empty"

        # ==== Flow ====
        if index-1 == len(self.__myList)-1 :
            # last element, pop out directly
            pass
        else:
            # not last element, 當前位置的值 = 後方位置的值
            for i in range(index-1, len(self.__myList)-1, 1):
                self.__myList[i] = self.__myList[i +1]

        # 移除最後一個元素
        self.__myList.pop()

        return self.__myList

    def _checkIndex(self, index:int) -> None:
        # wrong index type
        if type(index).__name__ != "int":
            assert False, "[Error] wrong index type, must be a int object"

        # wrong index start
        if index <= 0:
            assert False, "[Error] index must start from 1"

        # wrong index end
        if index > len(self.__myList):
            assert False, "[Error] out of index range"

# 取得兩個 List 之間 聯集的元素    
def union(a:MyList, b:MyList) -> MyList:
    '''
        思路，
            檢查短的array，
            對短array中的元素進行查找，
                若元素不存在長array中，插入到長array
                若元素存在長array中，pass
    '''
    long_list = None
    short_list = None

    # ==== find long_list & short_list ====
    len_a = a.getLength()
    len_b = b.getLength()

    if len_a >= len_b:
        short_list = b
        long_list = a
    else:
        short_list = a
        long_list = b

    # 查找元素是否重複，若否，插入新元素，若是，pass
    for item in short_list.mylist:
        index = long_list.getIndex(item)

        if index == None:
            long_list.mylist.append(item)   

    return long_list

if __name__ == "__main__":
    pass
