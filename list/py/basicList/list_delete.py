"""
功能
    刪除 list 中指定位置的元素
思路
    @Action
    #1  指定位置後方的元素往前(左)移動，a[i] = a[i+1]
    #2  透過 .pop() 將最後一個元素取出

    @Flow
    #1  檢查 index 是否合法
    #2  檢查 list 是否為空
    #3  將後方的元素往前移動

"""
from typing import List, Any

def delete_element(a:List[Any], index:int) -> List:
    # ==== check input list ====
    if len(a) == 0:
        assert False, "List is empty"

    # ==== check index ====
    if index >= len(a):
        assert False, "index is out of range"

    # ==== Flow ====
    if index == len(a)-1 :
        # last element, pop out directly
        pass
    else:
        # not last element, 當前位置的值 = 後方位置的值
        for i in range(index, len(a)-1, 1):
            a[i] = a[i +1]

    # 移除最後一個元素
    a.pop()

    return a


if __name__ == "__main__":
    print(delete_element([1,2,3], 2))

