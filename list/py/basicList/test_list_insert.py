import pytest
from list_insert import insert_element

def test_insert_element_usage():
    assert insert_element( [1,2,3,4], index=3, element=5) == [1,2,3,5,4]
    assert insert_element( [1,2,5], index=0, element="a") == ["a",1,2,5]

def test_insert_element_error():
    a = [1,2,3,4, 5]
    
    with pytest.raises(AssertionError, match="List is full, the MAX_SIZE of List is 5"):
        insert_element(a, 0, "a")

    with pytest.raises(AssertionError, match="List is full, the MAX_SIZE of List is 5"):
        insert_element(a, 5, "a")