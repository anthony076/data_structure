
function List(arr) {
    'use strict'
    this.data = arr || [];

    if (this.data.length > 1) {
        this.length = this.data.length
    } else {
        this.length = 0
    }

    this.pos = 0

    // =============
    // 常規操作
    // =============

    this.append = (el) => {
        if (typeof (el) == "object") {
            el.forEach(el => this.data[this.length++] = el)
        } else {
            this.data[this.length++] = el
        }

        return this.data
    }

    this.insert = (index, el) => {
        if (index == 0) {
            // 頭插
            this.data.unshift(el)
        } else if (index >= this.length) {
            // 尾插
            this.data[this.length] = el
        } else {
            // 中間插
            this.data.splice(index, 0, el)
        }

        ++this.length
        return this.data
    }

    this.clear = () => {
        this.data = []
        this.length = this.pos = 0
        return this.data
    }

    this.getElementByIndex = index => {
        if (index > this.length - 1) {
            throw new Error("index error")
        } else {
            return this.data[index]
        }
    }

    this.findIndex = el => {
        for (var i = 0; i < this.length; ++i) {
            if (this.data[i] == el) {
                return i
            }
        }

        return -1
    }

    this.isExist = el => {
        for (var i = 0; i < this.length; ++i) {
            if (this.data[i] == el) {
                return true
            }
        }

        return false
    }

    this.remove = el => {
        let index = this.findIndex(el)

        if (index == -1) {
            throw new Error("element not found")
        } else {
            this.data.splice(index, 1)
            --this.length
        }

        return this.data
    }

    // =============
    // 指標操作
    // =============
    this.currPos = () => {
        return this.pos
    }

    this.head = () => {
        this.pos = 0
        return this
    }

    this.end = () => {
        this.pos = this.length - 1
        return this
    }

    this.prev = () => {
        // not at head
        if (this.pos > 0) {
            --this.pos
        }
        return this
    }

    this.next = () => {
        ++this.pos
        return this
    }

    this.moveTo = (newPos) => {
        this.pos = newPos
        return this
    }

    this.getElement = () => this.data[this.pos]
}

const l = new List([1, 2, 3])
console.log(l.data)

for (l.head(); l.currPos() < l.length; l.next()) {
    console.log(l.getElement())
}


module.exports = { List }
































