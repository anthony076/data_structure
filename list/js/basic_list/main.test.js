const { List } = require("./main")

describe("Normal operation", () => {
    beforeEach(() => {
        myList = new List([])
        return myList
    })

    it("test append()", () => {
        expect(myList.append("a")).toEqual(["a"])
        expect(myList.length).toBe(1)
    })

    it("test clear()", () => {
        myList.append("a")
        expect(myList.clear()).toEqual([])
        expect(myList.length).toBe(0)
        expect(myList.pos).toBe(0)
    })

    it("test getElementByIndex()", () => {
        myList.append(["a", "b"])

        expect(myList.getElementByIndex(0)).toBe("a")
        expect(myList.getElementByIndex(1)).toBe("b")

        let errFn = () => myList.getElementByIndex(2)
        expect(errFn).toThrow("index error")
    })

    it("test findIndex()", () => {
        myList.append(["a", "b"])

        expect(myList.findIndex("b")).toBe(1)
        expect(myList.findIndex("c")).toBe(-1)
    })

    it("test remove()", () => {
        myList.append(["a", "b", "c"])
        expect(myList.remove("b")).toEqual(["a", "c"])
        expect(myList.length).toBe(2)

        expect(() => myList.remove("d")).toThrow("element not found")
    })

    it("test insert()", () => {
        myList.append(["a", "b", "c"])
        expect(myList.insert(0, "d")).toEqual(["d", "a", "b", "c"])
        expect(myList.insert(100, "e")).toEqual(["d", "a", "b", "c", "e"])
        expect(myList.insert(2, "f")).toEqual(["d", "a", "f", "b", "c", "e"])
    })

    it("test isExist()", () => {
        myList.append(["a", "b", "c"])
        expect(myList.isExist("a")).toBeTruthy()
        expect(myList.isExist("d")).toBeFalsy()
    })
})

describe("Pointer operation", () => {
    beforeEach(() => {
        myList = new List([])
        return myList
    })

    it("test currPos()", () => {
        myList.append(["a", "b", "c", "d"])
        myList.end()
        expect(myList.currPos()).toBe(3)
    })

    it("test cursor", () => {
        myList.append(["a", "b", "c", "d"])
        expect(myList.getElement()).toBe("a")
        expect(myList.next().getElement()).toBe("b")
        expect(myList.next().getElement()).toBe("c")
        expect(myList.prev().getElement()).toBe("b")
        expect(myList.head().getElement()).toBe("a")
        expect(myList.end().getElement()).toBe("d")

        myList.clear()
        myList.append(["a", "b", "c", "d"])
        expect(myList.moveTo(2).getElement()).toBe("c")
    })
})





